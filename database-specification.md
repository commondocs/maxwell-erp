## 数据库规范
### 1.动态字段
* 动态字段一般命名为json，数据定义:varchar(5000),顾名思义，里面存储的就是json字符串，用于表达非产品级的临时字段。
* 如果有分类明显的系列动态字段，也可以另外新建动态字段，命名为xxx_json。

### 2.表头顺序字段
* 表达表头顺序的字段统一为：system_table.head_number

### 3.数据顺序字段
* 表达数据顺序字段统一为：*.data_order  即任意表的data_order字段

### 4. 时间字段
* 每个表都应该有create_time,update_time两个时间字段，标识数据创建时间，更新时间
* 时间字段统一使用datetime类型，避免使用timestamp类型，因为其2038限制
* create_time,update_time两者默认值都为:CURRENT_TIMESTAMP
* update_time设置extra:on update CURRENT_TIMESTAMP，以便数据更新时update_time可以自动更新

### 5. 主键
* 主键不采用自动生成的方式，而是按照系统业务规则生成，主键定义：id varchar(50)

### 6. 命名
* 所有表名及字段名，如果是一个单词就使用一个单词即可，如果是多个单词，就使用下划线分割

### 7.全局唯一ID
* 系统采用自定规则生成全局唯一ID，纯数字ID,类型统一为：varchar(50).
* 不使用UUID/GUID。因为UUID/GUID难以表达数据含义，难以排序和索引

### 8 不支持项
* 原则上不支持表之外的其他数据库对象，包括但不仅限于：触发器，存储过程，视图等

### 9. 删除操作
* 全局使用软删除，应用不具备delete操作权限
* 每个表添加必须字段: `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除：0:否，1：是',

### 10.日志
* 所有关键操作都应该添加日志
* 日志表定义要求：将待操作数据转换成完整json保存到日志表；将具体操作完整描述到日志表；将所有操作对象完整描述到日志表
* 日志表一开始就需要按量分片策略
* 日志表按需清除步骤：分片表清除；分片ID重新初始化

### 11.关键字字段
* 数据库字段命名应该避开各层面的关键字，以免造成执行上的困难
* 如check可以用is_check命名，invalid可以用is_invalid命名