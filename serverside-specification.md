## 服务端规范
### 1. 异步请求
* 如无特殊要求，所有service层实现都使用异步

### 2.命名
* 驼峰命名

### 3.排序
* 每次传入一个排序字段，统一以属性：sortProperty 传入，后台统一解析
* sortOrder为排序方式传入，默认为asc，可选值有asc,desc
* sortProperty，sortOrder两个排序相关属性定义到所有实体类基类
* 原则上所有字段都默认支持排序
* 排序字段的解析统一放到支持分页的查询中，排序字段解析如下：
```
<if test="sortProperty != null">
  order by ${sortProperty}
</if>
<if test="sortOrder != null">
   ${sortOrder}
</if>
```
### 4.JSON动态字段
* 所有动态字段由json字段处理存取，定义以下规则：
* 为了简化处理，只保存一级结构，即只能直接存取key-value结构，不保存嵌套结构
* 定义统一的json字段处理接口，不能随意私自直接处理json字段
* json字段中的属性将与其它固定字段组成统一的数据集，对于用户而言就是同等数据集
* json字段各属性以结果集返回时，在服务端保存为map结构，key：propName,value:propValue
* json字段被编辑更新时，需取到json内属性的完整对象进行操作
* 存取json属性的时候，至少要保证：属性名，中文属性名，属性值，以便对应字段
* 所有传入的JSON属性都以"json_"为前缀，但是真实保存的json属性不保存这个前缀
* json字段的排序：只能在内存排序
* 防止json属性无故被删除或覆盖
* 每一个json属性的结构(以“客户名称”为例)：
```
{
    "propName": "customerName",//属性名称
    "propChineseName": "客户名称",//属性中文名称
    "propDataType": "String",//属性数据类型，为了简化，数据类型直接对应java数据类型
    "isDel": 0,//删除标记
    "propValue": "川普"//属性值
}
```
* 使用json属性的场景：
* 1.新建动态字段：
* 2.修改动态字段值：
* 3.删除动态字段数据：
* 4.查询动态字段数据：

### 5.系统脚本
* groovy风格，适当受限;groovy引擎执行
* 定义数据操作各函数(CRUD)
* 定义分页取数函数
* 定义统一返回对象以供实施调试
* 定义日志操作函数
* 流程控制说明(循环，判断等)
* 