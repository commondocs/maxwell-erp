```
# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: rm-uf6s5d9dmma05pq7v.mysql.rds.aliyuncs.com (MySQL 5.7.20-log)
# Database: maxwell
# Generation Time: 2019-05-19 02:27:44 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table base_bank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_bank`;

CREATE TABLE `base_bank` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `bank_name` varchar(100) DEFAULT NULL COMMENT '银行名称',
  `account` varchar(100) DEFAULT NULL COMMENT '银行账号',
  `is_private` bit(1) DEFAULT NULL COMMENT '是否私人卡',
  `bank_address` varchar(255) DEFAULT NULL COMMENT '银行地址',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_billno_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_billno_setting`;

CREATE TABLE `base_billno_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) DEFAULT NULL COMMENT '模块名称',
  `bill_name` varchar(100) DEFAULT NULL COMMENT '单据名称',
  `prefix_name` varchar(100) DEFAULT NULL COMMENT '前缀名称',
  `date_format` varchar(100) DEFAULT NULL COMMENT '日期格式',
  `pipeline_length` int(11) DEFAULT NULL COMMENT '流水长度',
  `is_del` bit(1) DEFAULT b'0',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_currency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_currency`;

CREATE TABLE `base_currency` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `name` varchar(50) DEFAULT NULL COMMENT '币种名称',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_customer_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_customer_level`;

CREATE TABLE `base_customer_level` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `customer_level` varchar(100) DEFAULT NULL COMMENT '审核人',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_depart
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_depart`;

CREATE TABLE `base_depart` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `depart_name` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `depart_no` varchar(50) DEFAULT NULL COMMENT '部门编号',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`),
  UNIQUE KEY `depart_no` (`depart_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_formulas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_formulas`;

CREATE TABLE `base_formulas` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `formula_name` varchar(100) DEFAULT NULL COMMENT '方案名称',
  `sort_name` int(11) DEFAULT NULL COMMENT '方案分类(0表示报价公式,1表示成本公式,2表示计件公式,3表示产能公式,4表示销售提成公式,5表示放损公式,6表示运费公式,7表示算料公式,7表示发外公式,8表示排程取数公式)',
  `description` varchar(255) DEFAULT NULL COMMENT '方案说明',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_group`;

CREATE TABLE `base_group` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `group_no` varchar(50) DEFAULT NULL COMMENT '组编号',
  `group_name` varchar(100) DEFAULT NULL COMMENT '组名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `base_group` WRITE;
/*!40000 ALTER TABLE `base_group` DISABLE KEYS */;

INSERT INTO `base_group` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `group_no`, `group_name`, `is_del`)
VALUES
	('20190421104353814380323',1,NULL,NULL,NULL,NULL,'1001','操作员',b'0'),
	('20190421180117130788880',2,NULL,'2019-04-21 23:41:11',NULL,NULL,'1002','二级管理',b'1');

/*!40000 ALTER TABLE `base_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table base_group_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_group_permission`;

CREATE TABLE `base_group_permission` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '权限-角色关联关系',
  `permission_id` varchar(50) DEFAULT NULL COMMENT '权限ID',
  `group_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `is_del` bit(1) DEFAULT b'0',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `base_group_permission` WRITE;
/*!40000 ALTER TABLE `base_group_permission` DISABLE KEYS */;

INSERT INTO `base_group_permission` (`id`, `permission_id`, `group_id`, `is_del`, `create_time`, `update_time`)
VALUES
	('2019042117054853336191','20190421170420291846149','20190421104353814380323',b'0','2019-04-21 17:05:48','2019-04-21 17:05:48');

/*!40000 ALTER TABLE `base_group_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table base_login_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_login_group`;

CREATE TABLE `base_login_group` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '用户-角色关联关系',
  `login_id` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `group_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除：0:否，1：是',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `base_login_group` WRITE;
/*!40000 ALTER TABLE `base_login_group` DISABLE KEYS */;

INSERT INTO `base_login_group` (`id`, `login_id`, `group_id`, `is_del`, `create_time`, `update_time`)
VALUES
	('20190425104321158429744','20190414111008535489551','20190421104353814380323',b'0','2019-04-25 10:43:21','2019-04-25 10:43:21'),
	('20190425141115606791203','20190414111008535489551','20190421104353814380323',b'0','2019-04-25 14:11:15','2019-04-25 14:11:15'),
	('20190425141230032802125','20190414111008535489551','20190421104353814380323',b'0','2019-04-25 14:12:30','2019-04-25 14:12:30'),
	('20190425141544817817422','20190414111008535489551','20190421104353814380323',b'0','2019-04-25 14:15:44','2019-04-25 14:15:44'),
	('20190425141544874124153','20190414111008535489551','20190421180117130788880',b'0','2019-04-25 14:15:44','2019-04-25 14:15:44');

/*!40000 ALTER TABLE `base_login_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table base_login_information
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_login_information`;

CREATE TABLE `base_login_information` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL COMMENT '员工id',
  `employee_id` varchar(50) DEFAULT NULL COMMENT '员工id',
  `employee_name` varchar(100) DEFAULT NULL COMMENT '员工姓名',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `user_password` varchar(255) DEFAULT NULL COMMENT '用户密码',
  `phone` varchar(100) DEFAULT NULL COMMENT '员工电话',
  `mobile` varchar(100) DEFAULT NULL COMMENT '员工手机',
  `inputmethod` varchar(100) DEFAULT NULL COMMENT '默认输入法',
  `super_user` bit(1) DEFAULT NULL COMMENT '是否超级用户',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `base_login_information` WRITE;
/*!40000 ALTER TABLE `base_login_information` DISABLE KEYS */;

INSERT INTO `base_login_information` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `json`, `employee_id`, `employee_name`, `user_name`, `user_password`, `phone`, `mobile`, `inputmethod`, `super_user`, `is_del`)
VALUES
	('20190414111008535489551',1,'2019-04-14 18:44:41','2019-04-14 18:44:41',NULL,NULL,NULL,NULL,NULL,'1000','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414111839345691385',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414114408686816936',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414114424710443165',4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,NULL,NULL,NULL,b'0'),
	('20190414114428987961377',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL,NULL,NULL,b'0'),
	('20190414114431113463292',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'12345','94ae0a96d83a445d72a93417b63ac90d79db5eca',NULL,NULL,NULL,NULL,b'0'),
	('20190414114433298162967',7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123457','dace72817a63023083233df9fb6d4f8617787408',NULL,NULL,NULL,NULL,b'0'),
	('2019041411443540169067',8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234579','bfa762464f8fdeaa771f49224e1edc94dadc1804',NULL,NULL,NULL,NULL,b'0'),
	('20190414114439635291029',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234579a','b0a9991ed636df381e31eb5886379f48cba96e89',NULL,NULL,NULL,NULL,b'0'),
	('20190414114517107253062',10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('2019041413531474941492',11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhf','1eccf22e2ee70666d6302ae942612a119ea90781',NULL,NULL,NULL,NULL,b'0'),
	('20190414135318727847633',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhf','1eccf22e2ee70666d6302ae942612a119ea90781',NULL,NULL,NULL,NULL,b'0'),
	('20190414135318877234887',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhf','1eccf22e2ee70666d6302ae942612a119ea90781',NULL,NULL,NULL,NULL,b'0'),
	('20190414135319031668940',14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhf','1eccf22e2ee70666d6302ae942612a119ea90781',NULL,NULL,NULL,NULL,b'0'),
	('20190414135319189279547',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhf','1eccf22e2ee70666d6302ae942612a119ea90781',NULL,NULL,NULL,NULL,b'0'),
	('20190414143047667303929',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143049476817491',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143049645591292',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143049821166838',19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143049996139426',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143050180162379',21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143050330126857',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('2019041414305053794666',23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143050676280321',24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143050869442478',25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143051215258009',26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143051390214625',27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143051562553161',28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143051756353956',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143051926224130',30,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414143055491544261',31,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'dwad','423e14cffea08eef9214663397ed1f3164af9f8c',NULL,NULL,NULL,NULL,b'0'),
	('20190414144037582963211',32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1qqq','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414144039979150700',33,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1qqq','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('2019041414404129290389',34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1qqq','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414144130311932877',35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1zhg','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414144139630242975',36,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1zhg111','356a192b7913b04c54574d18c28d46e6395428ab',NULL,NULL,NULL,NULL,b'0'),
	('20190414185228723790393',37,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'bluesx','6216f8a75fd5bb3d5f22b6f9958cdede3fc086c2',NULL,NULL,NULL,NULL,b'0'),
	('20190507151445320915062',38,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhangling','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,NULL,NULL,b'0'),
	('20190508133751393816482',39,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,NULL,NULL,b'0'),
	('20190508133753784542045',40,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'tx','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,NULL,NULL,b'0');

/*!40000 ALTER TABLE `base_login_information` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table base_look_center
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_look_center`;

CREATE TABLE `base_look_center` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `look_center_name` varchar(100) DEFAULT NULL COMMENT '查看中心',
  `formula_id` varchar(255) DEFAULT NULL COMMENT '方案id',
  `formula_name` varchar(255) DEFAULT NULL COMMENT '方案',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_machine
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_machine`;

CREATE TABLE `base_machine` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `name` varchar(50) DEFAULT NULL COMMENT '币种名称',
  `machine_no` varchar(50) DEFAULT NULL COMMENT '设备编号',
  `machine_name` varchar(100) DEFAULT NULL COMMENT '设备名称',
  `machine_status` int(11) DEFAULT NULL COMMENT '设备状态(0表示正常运行，1表示保养，2表示禁用维修，3表示返厂)',
  `depart_id` varchar(50) DEFAULT NULL COMMENT '所属部门id',
  `depart_name` varchar(100) DEFAULT NULL COMMENT '所属部门',
  `machine_type` int(11) DEFAULT NULL COMMENT '设备类型(0表示生产设备，1表示办公设备)',
  `printing` bit(1) DEFAULT NULL COMMENT '是否印刷设备',
  `purchase_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '购置时间',
  `planning_time_formula_id` varchar(50) DEFAULT NULL COMMENT '计划用时方案id',
  `planning_time_formula` varchar(100) DEFAULT NULL COMMENT '计划用时方案',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_menu`;

CREATE TABLE `base_menu` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '排序',
  `name` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(1000) DEFAULT NULL COMMENT '菜单路径',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT NULL COMMENT '删除，0：否，1：是',
  `desc` varchar(200) DEFAULT NULL COMMENT '菜单说明',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_page`;

CREATE TABLE `base_page` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '页面ID',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '排序',
  `name` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(1000) DEFAULT NULL COMMENT '菜单路径',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT NULL COMMENT '删除，0：否，1：是',
  `menu_desc` varchar(200) DEFAULT NULL COMMENT '菜单说明',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_page_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_page_data`;

CREATE TABLE `base_page_data` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '页面数据源ID',
  `page_id` varchar(50) DEFAULT NULL COMMENT '页面ID',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '排序',
  `datasource_name` varchar(200) DEFAULT NULL COMMENT '数据源名称',
  `url` varchar(1000) DEFAULT NULL COMMENT '数据源URL',
  `data_query` varchar(5000) DEFAULT NULL COMMENT '查询语句',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT NULL COMMENT '删除，0：否，1：是',
  `datasource_desc` varchar(200) DEFAULT NULL COMMENT '数据源说明',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_param
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_param`;

CREATE TABLE `base_param` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT NULL,
  `param_name` varchar(100) DEFAULT NULL COMMENT '参数名',
  `field_name` varchar(100) DEFAULT NULL COMMENT '字段名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_payment`;

CREATE TABLE `base_payment` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `payment_method` varchar(100) DEFAULT NULL COMMENT '付款方式',
  `type` int(11) DEFAULT NULL COMMENT '0表示送货后，1表示月结',
  `days` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL COMMENT '个月',
  `reconciliation_date` int(11) DEFAULT NULL COMMENT '对账日期   号/月',
  `reconciliation_day` int(11) DEFAULT NULL COMMENT '对账后   天付款',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_permissions`;

CREATE TABLE `base_permissions` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `permissions_name` varchar(100) DEFAULT NULL COMMENT '权限内容',
  `permissions_url` varchar(500) DEFAULT NULL COMMENT '权限路径',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级权限',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `base_permissions` WRITE;
/*!40000 ALTER TABLE `base_permissions` DISABLE KEYS */;

INSERT INTO `base_permissions` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `permissions_name`, `permissions_url`, `parent_id`, `is_del`)
VALUES
	('20190421170420291846149',1,NULL,NULL,NULL,NULL,'基础管理',NULL,NULL,b'0'),
	('20190422153415281180928',2,NULL,NULL,NULL,NULL,'基础管理1',NULL,'20190421170420291846149',b'0');

/*!40000 ALTER TABLE `base_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table base_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_position`;

CREATE TABLE `base_position` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `position_name` varchar(100) DEFAULT NULL COMMENT '职位',
  `depart_id` varchar(50) DEFAULT NULL COMMENT '部门id',
  `depart_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_salesman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_salesman`;

CREATE TABLE `base_salesman` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `employee_id` varchar(50) DEFAULT NULL COMMENT '员工id',
  `employee_name` varchar(100) DEFAULT NULL COMMENT '业务员',
  `salesman_no` varchar(50) DEFAULT NULL COMMENT '业务员编号',
  `commission_id` varchar(50) DEFAULT NULL COMMENT '业务员提成id',
  `commission_name` varchar(100) DEFAULT NULL COMMENT '业务员提成方案',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`),
  UNIQUE KEY `salesman_no` (`salesman_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_settlement_method
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_settlement_method`;

CREATE TABLE `base_settlement_method` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `settlement_method_name` varchar(100) DEFAULT NULL COMMENT '结算方式名称',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_system_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_system_settings`;

CREATE TABLE `base_system_settings` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `sbill_name` varchar(100) DEFAULT NULL COMMENT '单据名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_tax
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_tax`;

CREATE TABLE `base_tax` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `tax_group` varchar(100) DEFAULT NULL,
  `tax_rate` decimal(18,6) DEFAULT NULL COMMENT '税率',
  `tax_type` int(11) DEFAULT NULL COMMENT '0表示进项税，1表示销项税',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_team`;

CREATE TABLE `base_team` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `team_no` varchar(50) DEFAULT NULL COMMENT '班组编号',
  `team_name` varchar(100) DEFAULT NULL COMMENT '班组名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_team_emplyee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_team_emplyee`;

CREATE TABLE `base_team_emplyee` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `team_id` varchar(50) DEFAULT NULL COMMENT '班组id',
  `emplyee_id` varchar(50) DEFAULT NULL COMMENT '员工id',
  `emplyee_no` varchar(50) DEFAULT NULL,
  `emplyee_name` varchar(100) DEFAULT NULL COMMENT '员工名称',
  `proportion` decimal(18,6) DEFAULT NULL COMMENT '比例',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_team_machine
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_team_machine`;

CREATE TABLE `base_team_machine` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `team_id` varchar(50) DEFAULT NULL COMMENT '班组id',
  `machine_id` varchar(50) DEFAULT NULL COMMENT '设备id',
  `machine_no` varchar(50) DEFAULT NULL COMMENT '设备编号',
  `machine_name` varchar(100) DEFAULT NULL COMMENT '设备名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_unit`;

CREATE TABLE `base_unit` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `unit_no` varchar(20) DEFAULT NULL COMMENT '单位编号',
  `unit_name` varchar(20) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_ware_house
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_ware_house`;

CREATE TABLE `base_ware_house` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `ware_house_no` varchar(50) DEFAULT NULL COMMENT '仓库编号',
  `ware_house_name` varchar(100) DEFAULT NULL COMMENT '仓库名称',
  `ware_house_type_id` varchar(50) DEFAULT NULL COMMENT '仓库类型id',
  `ware_house_type_name` varchar(100) DEFAULT NULL COMMENT '仓库类型(物料，半成品，成品）',
  `manager_id` varchar(50) DEFAULT NULL COMMENT '管理员id',
  `manager_name` varchar(100) DEFAULT NULL COMMENT '管理员',
  `is_frozen` bit(1) DEFAULT NULL COMMENT '是否冻结(盘点时先冻结仓库)',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_ware_house_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_ware_house_type`;

CREATE TABLE `base_ware_house_type` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `type_no` varchar(50) DEFAULT NULL COMMENT '类型编号',
  `type_name` varchar(100) DEFAULT NULL COMMENT '仓库类型(物料，半成品，成品）',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table base_work_center
# ------------------------------------------------------------

DROP TABLE IF EXISTS `base_work_center`;

CREATE TABLE `base_work_center` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `work_center_name` varchar(100) DEFAULT NULL COMMENT '工作中心',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `customer_classify_id` varchar(50) DEFAULT NULL COMMENT '客户分类id',
  `customer_classify_name` varchar(100) DEFAULT NULL COMMENT '客户分类',
  `customer_no` varchar(50) NOT NULL COMMENT '客户编号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `customer_short` varchar(100) DEFAULT NULL COMMENT '客户简称',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `corporate` varchar(50) DEFAULT NULL COMMENT '公司法人',
  `register_capital` decimal(18,6) DEFAULT NULL COMMENT '注册资金',
  `company_address` varchar(255) DEFAULT NULL COMMENT '公司地址',
  `url` varchar(255) DEFAULT '' COMMENT '网址',
  `salesman_id` varchar(50) DEFAULT NULL COMMENT '业务员id',
  `salesman_name` varchar(100) DEFAULT NULL COMMENT '业务员',
  `customer_level` varchar(100) DEFAULT NULL COMMENT '客户等级',
  `payment_method_id` varchar(50) DEFAULT NULL COMMENT '付款方式id',
  `payment_method` varchar(100) DEFAULT NULL COMMENT '付款方式',
  `tax_group` varchar(100) DEFAULT NULL COMMENT '税收组',
  `tax_rate` decimal(18,6) DEFAULT NULL COMMENT '税率',
  `is_check` bit(1) DEFAULT NULL COMMENT '是否审核',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_no` (`customer_no`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_address`;

CREATE TABLE `customer_address` (
  `id` varchar(50) NOT NULL,
  `customer_id` varchar(50) DEFAULT NULL COMMENT '客户id',
  `consignee` varchar(20) DEFAULT NULL COMMENT '收货人',
  `consignee_mobile` varchar(50) DEFAULT '' COMMENT '收货人手机',
  `consignee_phone` varchar(100) DEFAULT NULL COMMENT '收货人电话',
  `region` varchar(20) DEFAULT NULL COMMENT '地区',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `area` varchar(20) DEFAULT NULL COMMENT '区',
  `detail_address` varchar(500) DEFAULT NULL COMMENT '详细地址',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '排序字段',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL COMMENT '预留字段',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_classify`;

CREATE TABLE `customer_classify` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分开名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_contacts`;

CREATE TABLE `customer_contacts` (
  `id` varchar(50) NOT NULL,
  `customer_id` varchar(50) DEFAULT NULL COMMENT '客户id',
  `scontacts` varchar(20) DEFAULT NULL COMMENT '联系人',
  `sposition` varchar(20) DEFAULT NULL COMMENT '职务',
  `mobile` varchar(50) DEFAULT '' COMMENT '手机',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `fax` varchar(20) DEFAULT NULL COMMENT '传真',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '排序字段',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `make_person` varchar(100) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(100) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL COMMENT '预留字段',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table employee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核日期',
  `employee_no` varchar(50) DEFAULT NULL COMMENT '员工编号',
  `employee_name` varchar(100) DEFAULT NULL,
  `iSex` int(11) DEFAULT NULL COMMENT '性别(0表示男，1表示女)',
  `depart_id` varchar(50) DEFAULT NULL COMMENT '部门id',
  `depart_name` varchar(100) DEFAULT NULL COMMENT '部门',
  `position_id` varchar(50) DEFAULT NULL COMMENT '职位id',
  `position_name` varchar(100) DEFAULT NULL COMMENT '职位',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机',
  `birthday_time` datetime DEFAULT NULL COMMENT '生日',
  `id_card_no` varchar(50) DEFAULT NULL COMMENT '身份证号码',
  `id_address` varchar(100) DEFAULT NULL COMMENT '身份证住址',
  `degree` varchar(100) DEFAULT NULL COMMENT '学历',
  `status` int(11) DEFAULT NULL COMMENT '状态(0表示入职，1表示离职)',
  `entry_time` datetime DEFAULT NULL COMMENT '入职日期',
  `departure_time` datetime DEFAULT NULL COMMENT '离职日期',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table material_common_style
# ------------------------------------------------------------

DROP TABLE IF EXISTS `material_common_style`;

CREATE TABLE `material_common_style` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '材料常用规格',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `common_style_name` varchar(100) DEFAULT NULL COMMENT '常用规格名称',
  `material_style` varchar(100) DEFAULT NULL COMMENT '材料规格',
  `material_length` decimal(18,6) DEFAULT NULL COMMENT '材料长',
  `material_width` decimal(18,6) DEFAULT NULL COMMENT '材料宽',
  `machine_style` varchar(100) DEFAULT NULL COMMENT '上机规格',
  `machine_length` decimal(18,6) DEFAULT NULL COMMENT '上机长',
  `machine_width` decimal(18,6) DEFAULT NULL COMMENT '上机宽',
  `materials_knum` decimal(18,6) DEFAULT NULL COMMENT '材料开数',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `materials`;

CREATE TABLE `materials` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `materials_no` varchar(100) NOT NULL COMMENT '物料编号',
  `materials_name` varchar(100) DEFAULT NULL COMMENT '物料名称',
  `purchase_price` decimal(18,6) DEFAULT NULL COMMENT '采购单价',
  `sales_price` decimal(18,6) DEFAULT NULL COMMENT '销售单价',
  `recent_purchase_price` decimal(18,6) DEFAULT NULL COMMENT '最近采购价',
  `purchase_unit` varchar(50) DEFAULT NULL COMMENT '采购单位',
  `gram_weight` decimal(18,6) DEFAULT NULL COMMENT '克重',
  `purchase_auxiliary_unit` varchar(50) DEFAULT NULL COMMENT '辅助单位',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `is_check` bit(1) DEFAULT NULL COMMENT '是否审核',
  `materials_type` int(11) DEFAULT NULL COMMENT '0表示主料，1表示辅料，2表示其他',
  `materials_classify_id` varchar(50) DEFAULT NULL COMMENT '物料分类id',
  `materials_classify_no` varchar(50) DEFAULT NULL COMMENT '物料分类编号',
  `materials_classify_name` varchar(100) DEFAULT NULL COMMENT '物料分类',
  `auto_compute` bit(1) DEFAULT NULL COMMENT '是否自动算料',
  `compute_id` varchar(50) DEFAULT NULL COMMENT '自动算料方案id',
  `compute_name` varchar(100) DEFAULT NULL COMMENT '自动算料方案',
  `is_web` bit(1) DEFAULT b'0' COMMENT '是否网络物料',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `materials_no` (`materials_no`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `materials` WRITE;
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;

INSERT INTO `materials` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `json`, `check_person`, `check_time`, `materials_no`, `materials_name`, `purchase_price`, `sales_price`, `recent_purchase_price`, `purchase_unit`, `gram_weight`, `purchase_auxiliary_unit`, `is_invalid`, `is_check`, `materials_type`, `materials_classify_id`, `materials_classify_no`, `materials_classify_name`, `auto_compute`, `compute_id`, `compute_name`, `is_web`, `is_del`)
VALUES
	('20190329',1,'2019-03-29 14:59:27','2019-05-13 14:59:05',NULL,NULL,NULL,NULL,'2019-05-13 14:59:05','WL0001','157g铜版纸',5500.000000,5600.000000,5500.000000,'吨',157.000000,'1',NULL,NULL,0,'20190329','01','铜版纸',NULL,NULL,NULL,NULL,b'1'),
	('20190506154831908130569',2,'2019-05-06 15:48:31','2019-05-13 15:10:57',NULL,NULL,NULL,NULL,'2019-05-13 15:10:57','10001','牛皮纸30g',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'20190403164719013124139',NULL,NULL,NULL,NULL,NULL,b'0',b'1'),
	('2019050715283950510491',3,'2019-05-07 15:28:39','2019-05-14 13:54:34',NULL,NULL,NULL,NULL,'2019-05-14 13:54:34','4','4',4.000000,NULL,NULL,'44',4.000000,'',NULL,NULL,6,'20190403164719012476736',NULL,'4',NULL,NULL,NULL,b'0',b'1'),
	('20190508134856229952013',4,'2019-05-08 13:48:56','2019-05-13 15:09:10',NULL,NULL,NULL,NULL,'2019-05-13 15:09:10','ma1020','哑粉纸',NULL,NULL,NULL,'',NULL,'',NULL,NULL,NULL,'20190403164718972445691',NULL,'',NULL,NULL,NULL,b'0',b'1'),
	('20190508153757190448967',5,'2019-05-08 15:37:57','2019-05-13 14:58:50',NULL,NULL,NULL,NULL,'2019-05-13 14:58:50','0','0',0.000000,NULL,NULL,'0',0.000000,'',NULL,NULL,99,'20190329',NULL,'0',NULL,NULL,NULL,b'0',b'1'),
	('20190509164015145100379',6,'2019-05-09 16:40:15','2019-05-09 16:40:15',NULL,NULL,NULL,NULL,NULL,'11','11',11.000000,NULL,NULL,'11',11.000000,'11',NULL,NULL,11,NULL,NULL,'11',NULL,NULL,NULL,b'0',b'0'),
	('20190509173312521528630',7,'2019-05-09 17:33:12','2019-05-09 17:33:22',NULL,NULL,NULL,NULL,'2019-05-09 17:33:22','random0.45884902306799225','哑粉纸',NULL,NULL,NULL,'',NULL,'',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,b'0',b'1'),
	('20190512092627267360151',8,'2019-05-12 09:26:27','2019-05-13 15:09:15',NULL,NULL,NULL,NULL,'2019-05-13 15:09:15','21','11',1.000000,NULL,NULL,'1',11.000000,'1',NULL,NULL,52,NULL,NULL,'2',NULL,NULL,NULL,b'0',b'1'),
	('20190512092720263101980',9,'2019-05-12 09:27:20','2019-05-13 14:58:08',NULL,NULL,NULL,NULL,'2019-05-13 14:58:08','1','',NULL,NULL,NULL,'',NULL,'',NULL,NULL,1,NULL,NULL,'1',NULL,NULL,NULL,b'0',b'1'),
	('20190512094209173776449',10,'2019-05-12 09:42:09','2019-05-12 09:51:19',NULL,NULL,NULL,NULL,'2019-05-12 09:51:19','2','4',4.000000,NULL,NULL,'2',4.000000,'4',NULL,NULL,12,NULL,NULL,'11',NULL,NULL,NULL,b'0',b'1'),
	('20190512094407645273368',11,'2019-05-12 09:44:07','2019-05-12 09:51:06',NULL,NULL,NULL,NULL,'2019-05-12 09:51:06','99','99',99.000000,NULL,NULL,'99',99.000000,'99',NULL,NULL,99,'20190329',NULL,'99',NULL,NULL,NULL,b'0',b'1'),
	('20190512095127666765721',12,'2019-05-12 09:51:27','2019-05-13 14:58:24',NULL,NULL,NULL,NULL,'2019-05-13 14:58:24','random0.056034401417031976','',NULL,NULL,NULL,'',NULL,'',NULL,NULL,1,NULL,NULL,'1',NULL,NULL,NULL,b'0',b'1'),
	('2019051209513426114613',13,'2019-05-12 09:51:34','2019-05-13 15:26:02',NULL,NULL,NULL,NULL,'2019-05-13 15:26:02','random0.9527546204462243','',NULL,NULL,NULL,'',NULL,'',NULL,NULL,1,NULL,NULL,'1',NULL,NULL,NULL,b'0',b'1'),
	('20190512103451021772780',14,'2019-05-12 10:34:51','2019-05-12 10:34:51',NULL,NULL,NULL,NULL,NULL,'45','77',74.000000,NULL,NULL,'77',85.000000,'7',NULL,NULL,25,NULL,NULL,'44',NULL,NULL,NULL,b'0',b'0'),
	('201905121035161735243',15,'2019-05-12 10:35:16','2019-05-12 10:35:16',NULL,NULL,NULL,NULL,NULL,'76','73',NULL,NULL,NULL,'3',7.000000,'7',NULL,NULL,68,NULL,NULL,'767',NULL,NULL,NULL,b'0',b'0'),
	('2019051215203187222014',16,'2019-05-12 15:20:31','2019-05-12 15:20:31',NULL,NULL,NULL,NULL,NULL,'09','09',9.000000,NULL,NULL,'09',NULL,'09',NULL,NULL,9,NULL,NULL,'09',NULL,NULL,NULL,b'0',b'0'),
	('20190512152740491786295',17,'2019-05-12 15:27:40','2019-05-12 15:27:47',NULL,NULL,NULL,NULL,'2019-05-12 15:27:47','10','10',10.000000,NULL,NULL,'10',10.000000,'10',NULL,NULL,10,'20190512112400377707417',NULL,'10',NULL,NULL,NULL,b'0',b'1'),
	('20190512154200707177825',19,'2019-05-12 15:42:00','2019-05-12 15:42:00',NULL,NULL,NULL,NULL,NULL,'111','111',111.000000,NULL,NULL,'111',111.000000,'111',NULL,NULL,111,'20190512112400377707417',NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190512154220297894410',20,'2019-05-12 15:42:20','2019-05-12 15:42:20',NULL,NULL,NULL,NULL,NULL,'222','222',222.000000,NULL,NULL,'222',222.000000,'222',NULL,NULL,222,'20190512112400377707417',NULL,'222',NULL,NULL,NULL,b'0',b'0'),
	('20190512154316991769181',21,'2019-05-12 15:43:16','2019-05-12 15:43:16',NULL,NULL,NULL,NULL,NULL,'333','333',333.000000,NULL,NULL,'333',333.000000,'333',NULL,NULL,333,'20190512112400377707417',NULL,'333',NULL,NULL,NULL,b'0',b'0'),
	('20190512154335751680059',22,'2019-05-12 15:43:35','2019-05-13 15:25:37',NULL,NULL,NULL,NULL,'2019-05-13 15:25:37','555','555',555.000000,NULL,NULL,'555',555.000000,'555',NULL,NULL,555,'20190512112400377707417',NULL,'555',NULL,NULL,NULL,b'0',b'1'),
	('20190512154348153933013',23,'2019-05-12 15:43:48','2019-05-13 15:25:47',NULL,NULL,NULL,NULL,'2019-05-13 15:25:47','666','666',666.000000,NULL,NULL,'666',666.000000,'666',NULL,NULL,666,'20190512112400377707417',NULL,'666',NULL,NULL,NULL,b'0',b'1'),
	('20190512160010586448145',24,'2019-05-12 16:00:10','2019-05-13 15:25:53',NULL,NULL,NULL,NULL,'2019-05-13 15:25:53','random0.46848418162106964','333',333.000000,NULL,NULL,'333',333.000000,'',NULL,NULL,333,NULL,NULL,'333',NULL,NULL,NULL,b'0',b'1'),
	('20190512160020111477009',25,'2019-05-12 16:00:20','2019-05-12 16:00:20',NULL,NULL,NULL,NULL,NULL,'random0.18592663872704374','666',666.000000,NULL,NULL,'666',666.000000,'',NULL,NULL,666,NULL,NULL,'666',NULL,NULL,NULL,b'0',b'0'),
	('20190512160030031947515',26,'2019-05-12 16:00:30','2019-05-12 16:00:30',NULL,NULL,NULL,NULL,NULL,'random0.10831042577848637','666',666.000000,NULL,NULL,'666',666.000000,'',NULL,NULL,666,NULL,NULL,'666',NULL,NULL,NULL,b'0',b'0'),
	('2019051216004540577648',27,'2019-05-12 16:00:45','2019-05-12 16:00:45',NULL,NULL,NULL,NULL,NULL,'random0.07103614061764385','111',111.000000,NULL,NULL,'111',111.000000,'',NULL,NULL,111,NULL,NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190512160135133782208',28,'2019-05-12 16:01:35','2019-05-12 16:01:35',NULL,NULL,NULL,NULL,NULL,'random0.05938806882320091','111',111.000000,NULL,NULL,'111',111.000000,'',NULL,NULL,111,NULL,NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190512160226502843941',29,'2019-05-12 16:02:26','2019-05-12 16:06:34',NULL,NULL,NULL,NULL,'2019-05-12 16:06:34','777','777',777.000000,NULL,NULL,'777',777.000000,'777',NULL,NULL,777,'20190512112400377707417',NULL,'777',NULL,NULL,NULL,b'0',b'1'),
	('20190512160542950637753',30,'2019-05-12 16:05:42','2019-05-12 16:05:42',NULL,NULL,NULL,NULL,NULL,'random0.41929262164449743','111',111.000000,NULL,NULL,'111',111.000000,'777',NULL,NULL,111,NULL,NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190512160617388816585',31,'2019-05-12 16:06:17','2019-05-12 16:06:17',NULL,NULL,NULL,NULL,NULL,'random0.18934808177433338','777',777.000000,NULL,NULL,'777',777.000000,'777',NULL,NULL,777,NULL,NULL,'777',NULL,NULL,NULL,b'0',b'0'),
	('20190512160945720568924',32,'2019-05-12 16:09:45','2019-05-12 16:09:45',NULL,NULL,NULL,NULL,NULL,'random0.3035967851604491','111',111.000000,NULL,NULL,'111',111.000000,'',NULL,NULL,111,'20190512112400377707417',NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190512162258617114170',33,'2019-05-12 16:22:58','2019-05-12 17:19:01',NULL,NULL,NULL,NULL,'2019-05-12 17:19:01','random0.9050899751370021','4',4.000000,NULL,NULL,'44',4.000000,'',NULL,NULL,6,'20190403164719012476736',NULL,'4',NULL,NULL,NULL,b'0',b'1'),
	('20190513100144394578893',34,'2019-05-13 10:01:44','2019-05-13 10:01:44',NULL,NULL,NULL,NULL,NULL,'12','12',12.000000,NULL,NULL,'12',NULL,'12',NULL,NULL,12,NULL,NULL,'12',NULL,NULL,NULL,b'0',b'0'),
	('20190513110222072282056',35,'2019-05-13 11:02:22','2019-05-13 11:02:22',NULL,NULL,NULL,NULL,NULL,'3','5',6.000000,NULL,NULL,'7',NULL,'5',NULL,NULL,1,NULL,NULL,'2',NULL,NULL,NULL,b'0',b'0'),
	('20190513163303379522218',36,'2019-05-13 16:33:03','2019-05-13 16:33:03',NULL,NULL,NULL,NULL,NULL,'random0.5773517500787941','4',4.000000,NULL,NULL,'44',4.000000,'',NULL,NULL,6,NULL,NULL,'4',NULL,NULL,NULL,b'0',b'0'),
	('20190513163316990790239',37,'2019-05-13 16:33:16','2019-05-13 16:33:16',NULL,NULL,NULL,NULL,NULL,'random0.6757272389357973','111',111.000000,NULL,NULL,'111',111.000000,'',NULL,NULL,111,NULL,NULL,'111',NULL,NULL,NULL,b'0',b'0'),
	('20190513163327501218242',38,'2019-05-13 16:33:27','2019-05-13 16:33:27',NULL,NULL,NULL,NULL,NULL,'random0.9371582613086893','5',6.000000,NULL,NULL,'7',NULL,'',NULL,NULL,1,NULL,NULL,'2',NULL,NULL,NULL,b'0',b'0'),
	('20190513173830636878907',39,'2019-05-13 17:38:30','2019-05-13 17:38:30',NULL,NULL,NULL,NULL,NULL,'22','22',22.000000,NULL,NULL,'22',22.000000,'22',NULL,NULL,22,'20190513131240218192959',NULL,'22',NULL,NULL,NULL,b'0',b'0'),
	('20190513175246267514093',40,'2019-05-13 17:52:46','2019-05-14 09:35:56',NULL,NULL,NULL,NULL,'2019-05-14 09:35:56','1111','1111',1111.000000,NULL,NULL,'111',11111.000000,'1',NULL,NULL,11,'20190513131240218192959',NULL,'111',NULL,NULL,NULL,b'0',b'1'),
	('20190514110842983755846',41,'2019-05-14 11:08:42','2019-05-14 11:08:42',NULL,NULL,NULL,NULL,NULL,'44','12',88.000000,NULL,NULL,'99',77.000000,'112',NULL,NULL,25,'20190513131240218192959',NULL,'21',NULL,NULL,NULL,b'0',b'0'),
	('20190514135440353320818',42,'2019-05-14 13:54:40','2019-05-14 13:54:40',NULL,NULL,NULL,NULL,NULL,'random0.19952175835316943','4',4.000000,NULL,NULL,'44',4.000000,'',NULL,NULL,6,NULL,NULL,'4',NULL,NULL,NULL,b'0',b'0'),
	('2019051414520067263349',43,'2019-05-14 14:52:00','2019-05-14 14:52:00',NULL,NULL,NULL,NULL,NULL,'random0.8876791633684664','11',11.000000,NULL,NULL,'11',11.000000,'',NULL,NULL,11,NULL,NULL,'11',NULL,NULL,NULL,b'0',b'0'),
	('2019051818210598277697',44,'2019-05-18 18:21:06','2019-05-18 18:21:06',NULL,NULL,NULL,NULL,NULL,'33','44',66.000000,NULL,NULL,'77',55.000000,'88',NULL,NULL,111,'20190513131240218192959',NULL,'22',NULL,NULL,NULL,b'0',b'0');

/*!40000 ALTER TABLE `materials` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table materials_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `materials_classify`;

CREATE TABLE `materials_classify` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `parent_id` varchar(50) DEFAULT '00' COMMENT '上级分类ID,-1为第一级',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `materials_classify` WRITE;
/*!40000 ALTER TABLE `materials_classify` DISABLE KEYS */;

INSERT INTO `materials_classify` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `classify_no`, `classify_name`, `parent_id`, `is_del`)
VALUES
	('20190329',725,'2019-04-03 16:51:12','2019-05-18 18:21:41',NULL,NULL,'01','铜版纸','20190403141047941982043',b'0'),
	('20190403141047941982043',727,'2019-04-03 16:51:34','2019-05-18 18:21:36',NULL,NULL,NULL,'工业纸板','20190329',b'0'),
	('20190403141511933680082',2,'2019-04-03 16:51:36','2019-05-12 11:21:59',NULL,NULL,NULL,'双铜','20190403140733156408614',b'0'),
	('20190403141528293960876',4,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'单粉','20190403140733156408614',b'0'),
	('20190403144904994797594',5,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'双面白','20190403140733156408614',b'0'),
	('20190403144905035134309',6,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'金卡','20190403140733156408614',b'0'),
	('20190403144905036653017',7,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'黑卡单涂','20190403140733156408614',b'0'),
	('20190403144905037270718',10,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'牛皮纸','20190403140733156408614',b'0'),
	('20190403144905037577048',11,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'书纸','20190403140733156408614',b'0'),
	('20190403144905038995767',12,'2019-04-03 16:51:41','2019-05-12 11:21:59',NULL,NULL,NULL,'特种纸','20190403140733156408614',b'0'),
	('20190403164718972445691',660,'2019-04-03 16:51:41','2019-05-13 09:35:23',NULL,NULL,NULL,'灰白','20190512105441893172987',b'0'),
	('20190403164719012476736',714,'2019-04-03 16:51:41','2019-05-13 13:02:09',NULL,NULL,'111','单白','20190512112239157233226',b'0'),
	('20190403164845822800548',718,'2019-04-03 16:51:41','2019-05-13 13:01:59',NULL,NULL,NULL,'磁铁','20190403164719012476736',b'0'),
	('20190403164845826279239',717,'2019-04-03 16:51:41','2019-05-13 13:01:59',NULL,NULL,NULL,'吸塑','20190403164719012476736',b'0'),
	('20190403164845827970400',719,'2019-04-03 16:51:41','2019-05-13 13:01:59',NULL,NULL,NULL,'EVA','20190403164719012476736',b'0'),
	('20190403164845828803129',720,'2019-04-03 16:51:41','2019-05-13 13:01:59',NULL,NULL,NULL,'胶片','20190403164719012476736',b'0'),
	('20190508163720219127112',678,'2019-05-08 16:37:20','2019-05-13 09:36:30',NULL,NULL,'1','1','20190403164718972445691',b'0'),
	('20190509173423916293999',713,'2019-05-09 17:34:24','2019-05-13 13:12:05',NULL,NULL,'11','124','00',b'1'),
	('20190512105441893172987',677,'2019-05-12 10:54:42','2019-05-13 09:36:30',NULL,NULL,'123','纯白','20190403164718972445691',b'0'),
	('20190512112150348847517',739,'2019-05-12 11:21:50','2019-05-19 00:06:00',NULL,NULL,'1','纸张','00',b'0'),
	('20190512112214153562666',715,'2019-05-12 11:22:14','2019-05-13 13:12:33',NULL,NULL,'2','单面','20190403141047941982043',b'0'),
	('20190512112239157233226',708,'2019-05-12 11:22:39','2019-05-13 13:01:46',NULL,NULL,'1','纸张','20190509173423916293999',b'0'),
	('20190512112400377707417',707,'2019-05-12 11:24:00','2019-05-13 13:01:46',NULL,NULL,'2','铜版纸','20190509173423916293999',b'0'),
	('20190512151754780206504',706,'2019-05-12 15:17:55','2019-05-13 13:01:46',NULL,NULL,'2','辅料','20190509173423916293999',b'0'),
	('20190512162014464770181',709,'2019-05-12 16:20:14','2019-05-13 13:01:46',NULL,NULL,'30','小纸板','20190509173423916293999',b'0'),
	('20190513131240218192959',737,'2019-05-13 13:12:40','2019-05-18 18:23:28',NULL,NULL,'3','双面','20190518182323104263060',b'0'),
	('20190518182157900877525',125,'2019-05-18 18:21:58','2019-05-18 18:21:58',NULL,NULL,'2','灰板','20190403141047941982043',b'0'),
	('20190518182221022794296',126,'2019-05-18 18:22:21','2019-05-18 18:22:21',NULL,NULL,'2','11','',b'0'),
	('20190518182231274109226',127,'2019-05-18 18:22:31','2019-05-18 18:22:35',NULL,NULL,'3','222','20190518182221022794296',b'0'),
	('20190518182239499626397',741,'2019-05-18 18:22:39','2019-05-19 00:06:11',NULL,NULL,'1','铜版纸','20190512112150348847517',b'0'),
	('2019051818225166427042',742,'2019-05-18 18:22:52','2019-05-19 00:06:11',NULL,NULL,'6','双胶纸','20190512112150348847517',b'0'),
	('20190518182307550779846',735,'2019-05-18 18:23:08','2019-05-18 18:23:36',NULL,NULL,'1','单面','20190518182323104263060',b'0'),
	('20190518182323104263060',740,'2019-05-18 18:23:23','2019-05-19 00:06:00',NULL,NULL,'8','灰卡','00',b'0'),
	('20190518182432600869970',743,'2019-05-18 18:24:33','2019-05-19 00:06:11',NULL,NULL,'4','哑粉纸','20190512112150348847517',b'0');

/*!40000 ALTER TABLE `materials_classify` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table process
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process`;

CREATE TABLE `process` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_no` varchar(50) DEFAULT NULL COMMENT '工序分类编号',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类',
  `process_classify_type` int(11) DEFAULT NULL COMMENT '0表示印前,1表示印刷,2表示印后,3表示成品',
  `process_name` varchar(100) DEFAULT NULL COMMENT '工序名称',
  `process_unit` varchar(20) DEFAULT NULL COMMENT '工序单位',
  `Valuation_Mode_id` varchar(50) DEFAULT NULL COMMENT '报价方案id',
  `Valuation_Mode` varchar(100) DEFAULT NULL COMMENT '报价方案',
  `arrange_process` bit(1) DEFAULT NULL COMMENT '是否排程工序',
  `outside_process` bit(1) DEFAULT NULL COMMENT '是否外发工序',
  `production_process` bit(1) DEFAULT NULL COMMENT '是否生产工序',
  `quotation_process` bit(1) DEFAULT NULL COMMENT '是否报价工序',
  `arrange_method_id` varchar(50) DEFAULT '' COMMENT '排程取数方案id',
  `arrange_method_name` varchar(100) DEFAULT '' COMMENT '排程取数方案',
  `work_center_id` varchar(50) DEFAULT NULL COMMENT '工作中心id',
  `work_center_name` varchar(100) DEFAULT NULL COMMENT '工作中心',
  `capacity_id` varchar(50) DEFAULT NULL COMMENT '工单产能方案id',
  `capacity_name` varchar(100) DEFAULT NULL COMMENT '工单产能方案',
  `look_center_id` varchar(50) DEFAULT NULL COMMENT '查看成本名称id',
  `look_center_name` varchar(100) DEFAULT NULL COMMENT '查看成本名称',
  `loss_formula_id` varchar(50) DEFAULT NULL COMMENT '工单放损方案id',
  `loss_formula_name` varchar(100) DEFAULT NULL COMMENT '工单放损方案',
  `quotation_loss_formula_id` varchar(50) DEFAULT NULL COMMENT '报价放损方案id',
  `quotation_loss_formula_name` varchar(100) DEFAULT NULL COMMENT '报价放损方案',
  `OutSideStyle` int(11) DEFAULT NULL COMMENT '发外规格(0表示取上机规格，1表示取部件规格，2表示取材料规格)',
  `printing_division` bit(1) DEFAULT NULL COMMENT '排程印刷是否分拆',
  `printing_after_division` bit(1) DEFAULT NULL COMMENT '排程印后是否分拆',
  `machine_max_length` decimal(18,6) DEFAULT NULL COMMENT '最大上机长度',
  `machine_max_width` decimal(18,6) DEFAULT NULL COMMENT '最大上机宽度',
  `machine_min_length` decimal(18,6) DEFAULT NULL COMMENT '最小上机长度',
  `machine_min_width` decimal(18,6) DEFAULT NULL COMMENT '最小上机宽度',
  `auto_arrange` bit(1) DEFAULT NULL COMMENT '是否自动排程',
  `invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `check` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table process_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process_classify`;

CREATE TABLE `process_classify` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `type` int(11) DEFAULT NULL COMMENT '0表示印前,1表示印刷,2表示印后,3表示成品'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table process_classify_slave
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process_classify_slave`;

CREATE TABLE `process_classify_slave` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_no` varchar(50) DEFAULT NULL COMMENT '工序分类编号',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_no` varchar(50) DEFAULT NULL COMMENT '产品分类编号',
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `select` bit(1) DEFAULT NULL COMMENT '是否选中'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table process_machine
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process_machine`;

CREATE TABLE `process_machine` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `machine_id` varchar(255) DEFAULT NULL COMMENT '机器id',
  `machine_no` varchar(50) DEFAULT NULL COMMENT '机器编号',
  `machine_name` varchar(100) DEFAULT NULL COMMENT '机器名',
  `default` bit(1) DEFAULT NULL COMMENT '是否默认机台'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table process_outside_formula
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process_outside_formula`;

CREATE TABLE `process_outside_formula` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `formula_id` varchar(50) DEFAULT NULL COMMENT '外发方案id',
  `formula_name` varchar(100) DEFAULT NULL COMMENT '外发方案',
  `formula_content` varchar(255) DEFAULT NULL COMMENT '外发方案内容',
  `supply_id` varchar(50) DEFAULT NULL COMMENT '供应商id',
  `supply_no` varchar(50) DEFAULT NULL COMMENT '供应商编号',
  `supply_name` varchar(100) DEFAULT NULL COMMENT '供应商名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table process_wage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `process_wage`;

CREATE TABLE `process_wage` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `wage_id` varchar(50) DEFAULT NULL COMMENT '实际计件方案id',
  `wage_name` varchar(100) DEFAULT NULL COMMENT '实际计件方案',
  `standard_cost_formula_id` varchar(50) DEFAULT NULL COMMENT '标准成本方案id',
  `standard_cost_formula_name` varchar(100) DEFAULT NULL COMMENT '标准成本方案'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `product_no` varchar(50) NOT NULL COMMENT '产品编号',
  `product_name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `customer_id` varchar(50) DEFAULT NULL COMMENT '客户id',
  `customer_no` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `product_style` varchar(100) DEFAULT NULL COMMENT '产品规格',
  `product_unit` varchar(50) DEFAULT NULL COMMENT '产品单位',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_no` varchar(50) DEFAULT NULL,
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `product_price` decimal(18,6) DEFAULT NULL COMMENT '产品单价',
  `common_style_id` varchar(50) DEFAULT NULL COMMENT '常用规格id',
  `common_style_name` varchar(100) DEFAULT NULL COMMENT '常用规格',
  `is_public` bit(1) DEFAULT NULL COMMENT '公共产品',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `is_check` bit(1) DEFAULT NULL,
  `volume` decimal(18,6) DEFAULT NULL,
  `weight` decimal(18,6) DEFAULT NULL,
  `customer_material_no` varchar(50) DEFAULT NULL COMMENT '客户物料号',
  `check_person` varchar(20) DEFAULT NULL,
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_no` (`product_no`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_classify`;

CREATE TABLE `product_classify` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `type` varchar(50) DEFAULT NULL COMMENT '所属分类',
  `unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `default_parts` varchar(255) DEFAULT NULL COMMENT '默认部件',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级分类ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`),
  UNIQUE KEY `classify_no` (`classify_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_classify` WRITE;
/*!40000 ALTER TABLE `product_classify` DISABLE KEYS */;

INSERT INTO `product_classify` (`id`, `data_order`, `create_time`, `update_time`, `make_person`, `modifier`, `classify_no`, `classify_name`, `type`, `unit`, `default_parts`, `is_del`, `parent_id`)
VALUES
	('20190508105120180151629',1,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL,b'0',NULL);

/*!40000 ALTER TABLE `product_classify` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_common_style
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_common_style`;

CREATE TABLE `product_common_style` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '产品常用规格',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `common_style_name` varchar(100) DEFAULT NULL COMMENT '常用规格名称',
  `product_style` varchar(100) DEFAULT NULL,
  `product_length` decimal(18,6) DEFAULT NULL COMMENT '产品长',
  `product_width` decimal(18,6) DEFAULT NULL COMMENT '产品宽',
  `product_height` decimal(18,6) DEFAULT NULL COMMENT '产品高',
  `expand_width` decimal(18,6) DEFAULT NULL COMMENT '展开宽/装订边高',
  `expand_length` decimal(18,6) DEFAULT NULL COMMENT '展开长',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_parts`;

CREATE TABLE `product_parts` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `parts_name` varchar(100) DEFAULT NULL COMMENT '部件名称',
  `sum_p_num` int(11) DEFAULT NULL COMMENT '默认P数',
  `expand_length_id` varchar(50) DEFAULT NULL COMMENT '展开长方案id',
  `expand_length_name` varchar(100) DEFAULT NULL COMMENT '展开长方案',
  `expand_width_id` varchar(50) DEFAULT NULL COMMENT '展开宽方案id',
  `expand_width_name` varchar(100) DEFAULT NULL COMMENT '展开宽方案',
  `auxiliary_materials` bit(1) DEFAULT b'0' COMMENT '是否辅料',
  `separate_stick` bit(1) DEFAULT NULL COMMENT '是否分贴',
  `expand_length` decimal(18,6) DEFAULT NULL COMMENT '展开长',
  `expand_width` decimal(18,6) DEFAULT NULL COMMENT '展开宽',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_parts_color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_parts_color`;

CREATE TABLE `product_parts_color` (
  `id` varchar(50) NOT NULL COMMENT '部件对应印色',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除，0否，1是',
  `color_name` varchar(100) DEFAULT NULL COMMENT '工序分类名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_parts_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_parts_materials`;

CREATE TABLE `product_parts_materials` (
  `id` varchar(50) NOT NULL COMMENT '部件对应材料',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除，0否，1是',
  `materials_classify_id` varchar(50) DEFAULT NULL COMMENT '物料分类id',
  `materials_classify_name` varchar(100) DEFAULT NULL COMMENT '物料分类名称',
  `materials_id` varchar(50) DEFAULT NULL COMMENT '物料id',
  `materials_no` varchar(50) DEFAULT NULL COMMENT '物料编号',
  `materials_name` varchar(100) DEFAULT NULL COMMENT '物料名称',
  `gram_weight` decimal(18,6) DEFAULT NULL COMMENT '克重',
  `purchase_unit` varchar(50) DEFAULT NULL COMMENT '采购单位',
  `purchase_price` decimal(18,6) DEFAULT NULL COMMENT '采购价格',
  `sales_price` decimal(18,6) DEFAULT NULL COMMENT '销售价',
  `recent_purchase_price` decimal(18,6) DEFAULT NULL COMMENT '最近采购价',
  `floating_coefficient` decimal(18,6) DEFAULT NULL COMMENT '上浮系数',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_parts_process
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_parts_process`;

CREATE TABLE `product_parts_process` (
  `id` varchar(50) NOT NULL COMMENT '部件对应工序',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除，0否，1是',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类名称',
  `process_classify_type` int(11) DEFAULT NULL COMMENT '工序分类',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `is_display` bit(1) DEFAULT NULL COMMENT '是否显示，0不显示，1显示',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_parts_surface
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_parts_surface`;

CREATE TABLE `product_parts_surface` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT b'0' COMMENT '是否删除，0否，1是',
  `surface_name` varchar(100) DEFAULT NULL COMMENT '印面名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process`;

CREATE TABLE `product_process` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_no` varchar(50) DEFAULT NULL COMMENT '工序分类编号',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类',
  `process_classify_type` int(11) DEFAULT NULL COMMENT '0表示印前,1表示印刷,2表示印后,3表示成品',
  `process_name` varchar(100) DEFAULT NULL COMMENT '工序名称',
  `process_unit` varchar(20) DEFAULT NULL COMMENT '工序单位',
  `valuation_mode_id` varchar(50) DEFAULT NULL COMMENT '报价方案id',
  `valuation_mode` varchar(100) DEFAULT NULL COMMENT '报价方案',
  `arrange_process` bit(1) DEFAULT NULL COMMENT '是否排程工序',
  `outside_process` bit(1) DEFAULT NULL COMMENT '是否外发工序',
  `production_process` bit(1) DEFAULT NULL COMMENT '是否生产工序',
  `quotation_process` bit(1) DEFAULT NULL COMMENT '是否报价工序',
  `arrange_method_id` varchar(50) DEFAULT '' COMMENT '排程取数方案id',
  `arrange_method_name` varchar(100) DEFAULT '' COMMENT '排程取数方案',
  `work_center_id` varchar(50) DEFAULT NULL COMMENT '工作中心id',
  `work_center_name` varchar(100) DEFAULT NULL COMMENT '工作中心',
  `capacity_id` varchar(50) DEFAULT NULL COMMENT '工单产能方案id',
  `capacity_name` varchar(100) DEFAULT NULL COMMENT '工单产能方案',
  `look_center_id` varchar(50) DEFAULT NULL COMMENT '查看成本名称id',
  `look_center_name` varchar(100) DEFAULT NULL COMMENT '查看成本名称',
  `loss_formula_id` varchar(50) DEFAULT NULL COMMENT '工单放损方案id',
  `loss_formula_name` varchar(100) DEFAULT NULL COMMENT '工单放损方案',
  `quotation_loss_formula_id` varchar(50) DEFAULT NULL COMMENT '报价放损方案id',
  `quotation_loss_formula_name` varchar(100) DEFAULT NULL COMMENT '报价放损方案',
  `outside_style` int(11) DEFAULT NULL COMMENT '发外规格(0表示取上机规格，1表示取部件规格，2表示取材料规格)',
  `printing_division` bit(1) DEFAULT NULL COMMENT '排程印刷是否分拆',
  `printing_after_division` bit(1) DEFAULT NULL COMMENT '排程印后是否分拆',
  `machine_max_length` decimal(18,6) DEFAULT NULL COMMENT '最大上机长度',
  `machine_max_width` decimal(18,6) DEFAULT NULL COMMENT '最大上机宽度',
  `machine_min_length` decimal(18,6) DEFAULT NULL COMMENT '最小上机长度',
  `machine_min_width` decimal(18,6) DEFAULT NULL COMMENT '最小上机宽度',
  `machine_max_colors` int(11) DEFAULT NULL COMMENT '机器最大印色',
  `machine_bite` int(11) DEFAULT NULL COMMENT '机器咬口',
  `machine_max_grameweight` int(11) DEFAULT NULL COMMENT '机器最大克重',
  `machine_min_grameweight` int(11) DEFAULT NULL COMMENT '机器最小克重',
  `auto_arrange` bit(1) DEFAULT NULL COMMENT '是否自动排程',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `is_check` bit(1) DEFAULT NULL,
  `machine_knum` int(11) unsigned zerofill DEFAULT NULL COMMENT '机器开数',
  `is_web` bit(1) DEFAULT b'0' COMMENT '是否网络工序',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  `is_assigned_area` bit(1) DEFAULT b'0' COMMENT '是否指定面积,单位统一为mm',
  `is_assigned_num` bit(1) DEFAULT b'0' COMMENT '是否指定数量',
  `assigned_num_unit` varchar(50) DEFAULT NULL COMMENT '指定数量单位',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_classify`;

CREATE TABLE `product_process_classify` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `type` int(11) DEFAULT NULL COMMENT '0表示印前,1表示印刷,2表示印后,3表示成品',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_classify_slave
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_classify_slave`;

CREATE TABLE `product_process_classify_slave` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_no` varchar(50) DEFAULT NULL COMMENT '工序分类编号',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_no` varchar(50) DEFAULT NULL COMMENT '产品分类编号',
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `choose` bit(1) DEFAULT b'0' COMMENT '是否选中',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_machine
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_machine`;

CREATE TABLE `product_process_machine` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `machine_id` varchar(255) DEFAULT NULL COMMENT '机器id',
  `machine_no` varchar(50) DEFAULT NULL COMMENT '机器编号',
  `machine_name` varchar(100) DEFAULT NULL COMMENT '机器名',
  `default` bit(1) DEFAULT NULL COMMENT '是否默认机台',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_outside_formula
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_outside_formula`;

CREATE TABLE `product_process_outside_formula` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `formula_id` varchar(50) DEFAULT NULL COMMENT '外发方案id',
  `formula_name` varchar(100) DEFAULT NULL COMMENT '外发方案',
  `formula_content` varchar(255) DEFAULT NULL COMMENT '外发方案内容',
  `supply_id` varchar(50) DEFAULT NULL COMMENT '供应商id',
  `supply_no` varchar(50) DEFAULT NULL COMMENT '供应商编号',
  `supply_name` varchar(100) DEFAULT NULL COMMENT '供应商名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_style
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_style`;

CREATE TABLE `product_process_style` (
  `id` varchar(50) NOT NULL COMMENT '工序对应规格',
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `material_style` varchar(100) DEFAULT NULL COMMENT '材料规格',
  `material_length` decimal(18,6) DEFAULT NULL COMMENT '材料长',
  `material_width` decimal(18,6) DEFAULT NULL COMMENT '材料宽',
  `machine_style` varchar(100) DEFAULT NULL COMMENT '上机规格',
  `machine_length` decimal(18,6) DEFAULT NULL COMMENT '上机长',
  `machine_width` decimal(18,6) DEFAULT NULL COMMENT '上机宽',
  `materials_knum` decimal(18,6) DEFAULT NULL COMMENT '材料开数',
  `machine_exact_style` varchar(100) DEFAULT NULL COMMENT 'ERP上机规格',
  `machine_exact_length` decimal(18,6) DEFAULT NULL COMMENT 'ERP上机长',
  `machine_exact_width` decimal(18,6) DEFAULT NULL COMMENT 'ERP上机宽',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table product_process_wage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_process_wage`;

CREATE TABLE `product_process_wage` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `wage_id` varchar(50) DEFAULT NULL COMMENT '实际计件方案id',
  `wage_name` varchar(100) DEFAULT NULL COMMENT '实际计件方案',
  `standard_cost_formula_id` varchar(50) DEFAULT NULL COMMENT '标准成本方案id',
  `standard_cost_formula_name` varchar(100) DEFAULT NULL COMMENT '标准成本方案',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation`;

CREATE TABLE `quotation` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `bill_code` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `customer_id` varchar(50) DEFAULT NULL COMMENT '客户id',
  `customer_no` varchar(50) DEFAULT NULL COMMENT '客户编号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `customer_short` varchar(100) DEFAULT NULL COMMENT '客户简称',
  `contacts` varchar(20) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `currency` varchar(100) DEFAULT NULL COMMENT '币种',
  `currency_rate` decimal(18,6) DEFAULT NULL COMMENT '币率',
  `payment_method_id` varchar(50) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `salesman_id` varchar(50) DEFAULT NULL,
  `salesman_name` varchar(100) DEFAULT NULL,
  `is_check` bit(1) DEFAULT NULL COMMENT '是否审核 1是，0否',
  `customer_level_id` varchar(50) DEFAULT NULL COMMENT '客户等级id',
  `customer_level_name` varchar(100) DEFAULT NULL COMMENT '客户等级',
  `is_del` bit(1) DEFAULT NULL,
  `deliver_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '交货日期',
  `json` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config`;

CREATE TABLE `quotation_config` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `check_person` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_no` varchar(50) DEFAULT NULL COMMENT '产品分类编号',
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `product_unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `bleeding` decimal(18,6) DEFAULT NULL,
  `up` decimal(18,6) DEFAULT NULL COMMENT '上↑',
  `down` decimal(18,6) DEFAULT NULL COMMENT '下↓',
  `left` decimal(18,6) DEFAULT NULL COMMENT '左←',
  `right` decimal(18,6) DEFAULT NULL COMMENT '右→',
  `corresponding_num` varchar(200) DEFAULT NULL COMMENT '对应数量',
  `custom_species_num` varchar(200) DEFAULT NULL COMMENT '自定义种数',
  `custom_size_display` bit(1) DEFAULT NULL COMMENT '自定义尺寸,0:不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_color`;

CREATE TABLE `quotation_config_color` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `color_name` varchar(100) DEFAULT NULL COMMENT '颜色名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `porduct_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_materials`;

CREATE TABLE `quotation_config_materials` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `materials_classify_id` varchar(50) DEFAULT NULL COMMENT '物料分类id',
  `materials_classify_name` varchar(100) DEFAULT NULL COMMENT '物料分类名称',
  `materials_id` varchar(50) DEFAULT NULL COMMENT '物料id',
  `materials_no` varchar(50) DEFAULT NULL COMMENT '物料编号',
  `materials_name` varchar(100) DEFAULT NULL COMMENT '物料名称',
  `gram_weight` decimal(18,6) DEFAULT NULL COMMENT '克重',
  `purchase_unit` varchar(50) DEFAULT NULL COMMENT '采购单位',
  `purchase_price` decimal(18,6) DEFAULT NULL COMMENT '采购价格',
  `sales_price` decimal(18,6) DEFAULT NULL COMMENT '销售价',
  `recent_purchase_price` decimal(18,6) DEFAULT NULL COMMENT '最近采购价',
  `floating_coefficient` decimal(18,6) DEFAULT NULL COMMENT '上浮系数',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_parts`;

CREATE TABLE `quotation_config_parts` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `surface_name` varchar(100) DEFAULT NULL COMMENT '印面名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `expand_length_id` varchar(50) DEFAULT NULL COMMENT '展开长方案id',
  `expand_length_name` varchar(100) DEFAULT NULL COMMENT '展开长方案',
  `expand_length` decimal(18,6) DEFAULT NULL COMMENT '展开长',
  `expand_width_id` varchar(50) DEFAULT NULL COMMENT '展开宽方案id',
  `expand_width_name` varchar(100) DEFAULT NULL COMMENT '展开宽方案',
  `expand_width` decimal(18,6) DEFAULT NULL COMMENT '展开宽',
  `product_length` decimal(18,6) DEFAULT NULL COMMENT '产品长',
  `product_width` decimal(18,6) DEFAULT NULL COMMENT '产品宽',
  `product_height` decimal(18,6) DEFAULT NULL COMMENT '产品高',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_process
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_process`;

CREATE TABLE `quotation_config_process` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类名称',
  `process_classify_type` int(11) DEFAULT NULL COMMENT '工序分类，1：印刷，2：印后，3：成品',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `is_display` bit(1) DEFAULT NULL COMMENT '是否显示，0不显示，1显示',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_style
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_style`;

CREATE TABLE `quotation_config_style` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `common_style_id` varchar(50) DEFAULT NULL COMMENT '常用规格id',
  `common_style_name` varchar(100) DEFAULT NULL COMMENT '常用规格名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `expand_width` decimal(18,6) DEFAULT NULL COMMENT '展开宽/装订边高',
  `expand_length` decimal(18,6) DEFAULT NULL COMMENT '展开长',
  `product_length` decimal(18,6) DEFAULT NULL COMMENT '产品长',
  `product_width` decimal(18,6) DEFAULT NULL COMMENT '产品宽',
  `product_height` decimal(18,6) DEFAULT NULL COMMENT '产品高',
  `product_classify_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_config_surface
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_config_surface`;

CREATE TABLE `quotation_config_surface` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `surface_name` varchar(100) DEFAULT NULL COMMENT '印面名称',
  `product_parts_id` varchar(50) DEFAULT NULL COMMENT '对应部件id(产品部件表id）',
  `product_parts_name` varchar(100) DEFAULT NULL COMMENT '对应部件名称(产品部件表部件名称）',
  `product_classify_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_detail`;

CREATE TABLE `quotation_detail` (
  `id` varchar(50) NOT NULL,
  `quotation_id` varchar(50) DEFAULT NULL COMMENT '主表id',
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `product_id` varchar(50) DEFAULT NULL COMMENT '产品id',
  `product_no` varchar(50) DEFAULT NULL COMMENT '产品编号',
  `product_name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `product_style` varchar(100) DEFAULT NULL COMMENT '产品规格',
  `product_unit` varchar(50) DEFAULT NULL COMMENT '单位',
  `product_price` decimal(18,6) DEFAULT NULL COMMENT '产品单价',
  `product_money` decimal(18,6) DEFAULT NULL COMMENT '产品金额',
  `profit_rate` decimal(18,6) DEFAULT NULL COMMENT '利润率%',
  `profit_money` decimal(18,6) DEFAULT NULL COMMENT '利润',
  `cost_price` decimal(18,6) DEFAULT NULL COMMENT '报价成本单价',
  `cost_money` decimal(18,6) DEFAULT NULL COMMENT '报价成本',
  `all_money` decimal(18,6) DEFAULT NULL COMMENT '总金额',
  `is_del` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_materials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_materials`;

CREATE TABLE `quotation_materials` (
  `id` varchar(50) NOT NULL,
  `quotation_id` varchar(50) DEFAULT NULL COMMENT '主表id',
  `quotation_detail_id` varchar(50) DEFAULT NULL COMMENT '从表id',
  `parts_id` varchar(50) DEFAULT NULL COMMENT '部件表id',
  `parts_name` varchar(100) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `materials_classify_id` varchar(50) DEFAULT NULL COMMENT '物料分类id',
  `materials_classify_name` varchar(100) DEFAULT NULL COMMENT '物料分类名称',
  `materials_id` varchar(50) DEFAULT NULL COMMENT '物料id',
  `materials_no` varchar(50) DEFAULT NULL COMMENT '物料编号',
  `materials_name` varchar(100) DEFAULT NULL COMMENT '物料名称',
  `materials_style` varchar(100) DEFAULT NULL COMMENT '物料规格',
  `machine_style` varchar(100) DEFAULT NULL COMMENT '上机规格',
  `materials_open_num` int(11) DEFAULT NULL COMMENT '物料开数',
  `gram_weight` decimal(18,6) DEFAULT NULL COMMENT '克重',
  `auxiliary_num` int(11) DEFAULT NULL,
  `purchase_auxiliary_unit` varchar(255) DEFAULT NULL,
  `materials_num` int(11) DEFAULT NULL,
  `purchase_unit` varchar(50) DEFAULT NULL COMMENT '采购单位',
  `materials_price` decimal(18,6) DEFAULT NULL COMMENT '物料价格',
  `materials_money` decimal(18,6) DEFAULT NULL COMMENT '物料金额',
  `coefficient` decimal(18,6) DEFAULT NULL COMMENT '系数',
  `type` int(11) DEFAULT NULL COMMENT '类型(0表示部件材料表，1表示成品辅料表）',
  `is_incoming_material` bit(1) DEFAULT NULL COMMENT '是否来料（0表示不是，1表示1）',
  `process_id` varchar(50) DEFAULT NULL COMMENT '对应工序id',
  `process_name` varchar(100) DEFAULT NULL COMMENT '对应工序',
  `product_classify_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_parts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_parts`;

CREATE TABLE `quotation_parts` (
  `id` varchar(50) NOT NULL,
  `quotation_id` varchar(50) DEFAULT NULL COMMENT '主表id',
  `quotation_detail_id` varchar(50) DEFAULT NULL COMMENT '从表id',
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `json` varchar(5000) DEFAULT NULL,
  `parts_name` varchar(100) DEFAULT NULL COMMENT '部件名称',
  `parts_num` int(11) DEFAULT NULL COMMENT '部件数量',
  `parts_style` varchar(100) DEFAULT NULL COMMENT '部件规格',
  `sum_p_num` int(11) DEFAULT NULL COMMENT '部件P数',
  `imposition_num` int(11) DEFAULT NULL COMMENT '拼版数',
  `expand_length` decimal(18,6) DEFAULT NULL COMMENT '展开长',
  `expand_width` decimal(18,6) DEFAULT NULL COMMENT '展开宽',
  `positive_color` int(11) DEFAULT NULL COMMENT '正面颜色',
  `positive_special_color` int(11) DEFAULT NULL COMMENT '正面专色',
  `opposite_color` int(11) DEFAULT NULL COMMENT '反面颜色',
  `opposite_specia_color` int(11) DEFAULT NULL COMMENT '反面专色',
  `surface_name` varchar(100) DEFAULT NULL COMMENT '印刷面数（单面，双面）',
  `print_mode` int(11) DEFAULT NULL COMMENT '印刷方式（0正反版，1左右翻，2天地翻，3单面印刷，4无需印刷）',
  `machine_num` int(11) DEFAULT NULL COMMENT '印张正数(单贴)',
  `sum_machine_num` int(11) DEFAULT NULL COMMENT '总印张正数(单贴)',
  `stick` int(11) DEFAULT NULL COMMENT '贴数',
  `plate_num` int(11) DEFAULT NULL COMMENT '印版付数',
  `sum_plate_num` int(11) DEFAULT NULL COMMENT '印版张数',
  `print_mode_memo` varchar(100) DEFAULT NULL COMMENT '印版方式备注（表示为 1贴正反印, 贴数+印刷方式）',
  `print_loss_num` int(11) DEFAULT NULL COMMENT '印刷损耗数量(单贴）',
  `print_after_loss_num` int(11) DEFAULT NULL COMMENT '印后损耗数量(单贴）',
  `sum_loss_num` int(11) DEFAULT NULL COMMENT '总放损数量 (印刷放损+印后放损）*贴数',
  `is_del` bit(1) DEFAULT NULL,
  `product_classify_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table quotation_process
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quotation_process`;

CREATE TABLE `quotation_process` (
  `id` varchar(50) NOT NULL,
  `quotation_id` varchar(50) DEFAULT NULL,
  `quotation_detail_id` varchar(50) DEFAULT NULL,
  `parts_id` varchar(50) DEFAULT NULL,
  `parts_name` varchar(100) DEFAULT NULL,
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `json` varchar(5000) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  `process_classify_id` varchar(50) DEFAULT NULL COMMENT '工序分类id',
  `process_classify_name` varchar(100) DEFAULT NULL COMMENT '工序分类名称',
  `process_classify_type` int(11) DEFAULT NULL COMMENT '工序分类',
  `process_id` varchar(50) DEFAULT NULL COMMENT '工序id',
  `process_name` varchar(100) DEFAULT NULL COMMENT '工序名称',
  `process_unit` varchar(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '类型，0印前工序，1印刷工序，2印后工序，3成品工序',
  `outside_process` bit(1) DEFAULT NULL COMMENT '外发工序(0表示不外发，1表示外发）',
  `machine_id` varchar(50) DEFAULT NULL COMMENT '机器id',
  `machine_no` varchar(50) DEFAULT NULL COMMENT '机器编号',
  `machine_name` varchar(100) DEFAULT NULL COMMENT '机器名称',
  `process_price` decimal(18,6) DEFAULT NULL COMMENT '工序单价',
  `process_money` decimal(18,6) DEFAULT NULL COMMENT '工序金额',
  `valuation_mode_id` varchar(50) DEFAULT NULL COMMENT '报价方案id',
  `valuation_mode` varchar(100) DEFAULT NULL COMMENT '报价方案',
  `input_num` decimal(18,6) DEFAULT NULL COMMENT '投料数',
  `loss_num` decimal(18,6) DEFAULT NULL COMMENT '损耗数',
  `output_num` decimal(18,6) DEFAULT NULL COMMENT '产出数',
  `adjustment_num` decimal(18,6) DEFAULT NULL COMMENT '调整数',
  `difficulty_coefficient` decimal(18,6) DEFAULT NULL COMMENT '难易系数',
  `rate` decimal(18,6) DEFAULT NULL COMMENT '比率，（产出/投料，产出是投料的几倍）',
  `product_classify_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sales_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_order`;

CREATE TABLE `sales_order` (
  `id` varchar(20) NOT NULL,
  `bill_code` varchar(100) NOT NULL COMMENT '单据编号',
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `make_person` varchar(100) DEFAULT NULL COMMENT '制单人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modifer` varchar(100) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `check_person` varchar(100) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核日期',
  `is_check` bit(1) DEFAULT NULL COMMENT '是否审核，1是，0否',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废，1是，0否',
  `is_del` bit(1) DEFAULT NULL COMMENT '删除标记 1是，0否',
  `is_print` bit(1) DEFAULT NULL COMMENT '删除打印 1是，0否',
  `customer_id` varchar(50) DEFAULT NULL,
  `customer_no` varchar(100) DEFAULT NULL COMMENT '客户编号',
  `customer_name` varchar(100) DEFAULT NULL COMMENT '客户姓名',
  `customer_short` varchar(100) DEFAULT NULL COMMENT '客户简称',
  `currency` varchar(20) DEFAULT NULL COMMENT '币种',
  `currency_rate` decimal(18,6) DEFAULT NULL COMMENT '币率',
  `contacts` varchar(100) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `fax` varchar(100) DEFAULT NULL COMMENT '传真',
  `sale_man_id` varchar(50) DEFAULT NULL COMMENT '业务员id',
  `sale_man_name` varchar(100) DEFAULT NULL COMMENT '业务员',
  `customer_order_no` varchar(100) DEFAULT NULL COMMENT '客户订单号',
  `earnest money` decimal(18,6) DEFAULT NULL COMMENT '定金',
  `tax_money` decimal(18,6) DEFAULT NULL COMMENT '税额',
  `total_money` decimal(18,6) DEFAULT NULL COMMENT '总金额',
  `payment_method_id` varchar(50) DEFAULT NULL COMMENT '付款方式id',
  `payment_method` varchar(100) DEFAULT NULL COMMENT '付款方式',
  `pay_date` datetime DEFAULT NULL COMMENT '付款日期',
  `deliver_date` datetime DEFAULT NULL COMMENT '交货日期',
  `deliver_mode_id` varchar(50) DEFAULT NULL COMMENT '送货方式id',
  `deliver_mode` varchar(100) DEFAULT NULL COMMENT '送货方式',
  `deliver_address` varchar(1000) DEFAULT NULL COMMENT '送货地址',
  `consignee` varchar(100) DEFAULT NULL COMMENT '收货人',
  `consignee_mobile` varchar(100) DEFAULT NULL COMMENT '收货人手机',
  `consignee_phone` varchar(100) DEFAULT NULL COMMENT '收货人电话',
  `json` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sales_order_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_order_detail`;

CREATE TABLE `sales_order_detail` (
  `id` varchar(50) NOT NULL,
  `sales_id` varchar(50) DEFAULT NULL COMMENT '销售订单主表id',
  `data_order` int(11) DEFAULT NULL COMMENT '序号',
  `product_classify_id` varchar(50) DEFAULT NULL COMMENT '产品分类id',
  `product_classify_no` varchar(50) DEFAULT NULL COMMENT '产品分类编号',
  `product_classify_name` varchar(100) DEFAULT NULL COMMENT '产品分类',
  `product_id` varchar(50) DEFAULT NULL COMMENT '产品id',
  `product_no` varchar(100) DEFAULT NULL COMMENT '产品编号',
  `product_name` varchar(1000) DEFAULT NULL COMMENT '产品名称',
  `product_style` varchar(100) DEFAULT NULL COMMENT '产品规格',
  `product_unit` varchar(100) DEFAULT NULL COMMENT '单位',
  `customer_material_no` varchar(100) DEFAULT NULL COMMENT '客户料号',
  `product_num` decimal(18,6) DEFAULT NULL COMMENT '产品数量',
  `inventory_num` decimal(18,6) DEFAULT NULL COMMENT '库存数量',
  `product_no_tax_price` decimal(18,6) DEFAULT NULL COMMENT '不含税单价',
  `product_no_tax_money` decimal(18,6) DEFAULT NULL COMMENT '不含税金额',
  `tax_group` varchar(100) DEFAULT NULL COMMENT '税收组',
  `tax_rate` decimal(18,6) DEFAULT NULL COMMENT '税率',
  `product_tax_money` decimal(18,6) DEFAULT NULL COMMENT '产品税额',
  `product_price` decimal(18,6) DEFAULT NULL COMMENT '产品单价',
  `product_money` decimal(18,6) DEFAULT NULL COMMENT '产品金额',
  `quotation_id` varchar(50) DEFAULT NULL,
  `quotation_code` varchar(100) DEFAULT NULL COMMENT '报价单号',
  `quotation_num` decimal(18,6) DEFAULT NULL,
  `quotation_price` decimal(18,6) DEFAULT NULL COMMENT '报价单价',
  `quotation_money` decimal(18,6) DEFAULT NULL COMMENT '报价金额',
  `
production_order_num` decimal(18,6) DEFAULT NULL COMMENT '生产数量',
  `deliver_goods_num` decimal(18,6) DEFAULT NULL COMMENT '送货数量',
  `deliver_notify_num` decimal(18,6) DEFAULT NULL COMMENT '送货通知数量',
  `receipt_money` decimal(18,6) DEFAULT NULL COMMENT '收款金额',
  `is_production_order_complete` bit(1) DEFAULT NULL COMMENT '是否工单完成  1是0否',
  `is_deliver_goods_complete` bit(1) DEFAULT NULL COMMENT '是否送货完成  1是0否',
  `is_deliver_notify_complete` bit(1) DEFAULT NULL COMMENT '是否送货通知完成  1是0否',
  `deliver_time` datetime DEFAULT NULL COMMENT '交货日期',
  `production_order_code` varchar(100) DEFAULT NULL COMMENT '工单编号',
  `production_order_time` datetime DEFAULT NULL COMMENT '工单时间',
  `product_parent_name` varchar(100) DEFAULT NULL,
  `convert_num` decimal(18,6) DEFAULT NULL COMMENT '换算数量',
  `convert_price` decimal(18,6) DEFAULT NULL COMMENT '换算单价',
  `pack_mode_id` varchar(50) DEFAULT NULL COMMENT '包装方式',
  `pack_mode` varchar(100) DEFAULT NULL COMMENT '包装方式',
  `pack_num` decimal(18,6) DEFAULT NULL COMMENT '包装数量',
  `common_style_id` varchar(50) DEFAULT NULL,
  `source_id` varchar(50) DEFAULT NULL COMMENT '源单主表id',
  `source_detail_id` varchar(50) DEFAULT NULL COMMENT '源单明细表id',
  `source_num` decimal(18,6) DEFAULT NULL COMMENT '源单数量',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  `json` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supply
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supply`;

CREATE TABLE `supply` (
  `id` varchar(50) NOT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `supply_classify_id` varchar(50) DEFAULT NULL COMMENT '供应商分类id',
  `supply_classify_name` varchar(100) DEFAULT NULL COMMENT '供应商分类',
  `supply_no` varchar(50) NOT NULL COMMENT '供应商编号',
  `supply_name` varchar(100) DEFAULT NULL COMMENT '供应商名称',
  `supply_short` varchar(100) DEFAULT NULL COMMENT '供应商简称',
  `currency` varchar(50) DEFAULT NULL COMMENT '币种',
  `corporate` varchar(50) DEFAULT NULL COMMENT '公司法人',
  `register_capital` decimal(18,6) DEFAULT NULL COMMENT '注册资金',
  `company_address` varchar(255) DEFAULT NULL COMMENT '公司地址',
  `url` varchar(255) DEFAULT '' COMMENT '网址',
  `purchase_man_id` varchar(50) DEFAULT NULL COMMENT '采购员id',
  `purchase_man_name` varchar(100) DEFAULT NULL COMMENT '采购员',
  `payment_method_id` varchar(50) DEFAULT NULL COMMENT '付款方式id',
  `payment_method` varchar(100) DEFAULT NULL COMMENT '付款方式',
  `tax_group` varchar(100) DEFAULT NULL COMMENT '税收组',
  `tax_rate` decimal(18,6) DEFAULT NULL COMMENT '税率',
  `is_check` bit(1) DEFAULT NULL COMMENT '是否审核',
  `is_invalid` bit(1) DEFAULT NULL COMMENT '是否作废',
  `json` varchar(5000) DEFAULT NULL,
  `check_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核日期',
  `check_penson` varchar(20) DEFAULT NULL COMMENT '审核人',
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `contacts` varchar(20) DEFAULT NULL,
  `is_del` bit(1) DEFAULT NULL COMMENT '是否删除，0否，1是',
  PRIMARY KEY (`id`),
  UNIQUE KEY `supply_no` (`supply_no`),
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supply_classify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supply_classify`;

CREATE TABLE `supply_classify` (
  `id` varchar(50) DEFAULT NULL,
  `data_order` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `make_person` varchar(20) DEFAULT NULL COMMENT '制单人',
  `modifier` varchar(20) DEFAULT NULL COMMENT '修改人',
  `classify_no` varchar(50) DEFAULT NULL COMMENT '分类编号',
  `classify_name` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  UNIQUE KEY `data_order` (`data_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table system_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_config`;

CREATE TABLE `system_config` (
  `id` varchar(50) DEFAULT NULL,
  `item_type` varchar(100) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_desc` varchar(200) DEFAULT NULL,
  `item_url` varchar(500) DEFAULT NULL,
  `parent_id` varchar(50) DEFAULT NULL,
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标记',
  `table_name` varchar(200) DEFAULT NULL,
  `table_type` varchar(100) DEFAULT NULL,
  `table_query` varchar(5000) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `system_config` WRITE;
/*!40000 ALTER TABLE `system_config` DISABLE KEYS */;

INSERT INTO `system_config` (`id`, `item_type`, `item_name`, `item_desc`, `item_url`, `parent_id`, `is_del`, `table_name`, `table_type`, `table_query`, `create_time`, `update_time`)
VALUES
	('1','display','销售订单',NULL,NULL,NULL,b'0',NULL,NULL,NULL,NULL,'2019-04-22 23:34:33'),
	('2','display','销售订单主表',NULL,NULL,'1',b'0','sales_order','view',NULL,NULL,'2019-04-22 23:34:33'),
	('3','display','销售订单从表',NULL,NULL,'1',b'0',NULL,NULL,NULL,NULL,'2019-04-22 23:34:33'),
	('4','display','销售订单明细',NULL,NULL,'1',b'0',NULL,NULL,NULL,NULL,'2019-04-22 23:34:33'),
	('5','display','客户信息',NULL,NULL,NULL,b'0',NULL,NULL,NULL,NULL,'2019-04-22 23:34:33'),
	('6','display','客户信息主表',NULL,NULL,'5',b'0','customer','table','select * from customer',NULL,'2019-04-22 23:34:33'),
	('7','display','材料信息表',NULL,NULL,NULL,b'0','materials','table','select * from materials',NULL,'2019-04-22 23:34:33');

/*!40000 ALTER TABLE `system_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table system_table
# ------------------------------------------------------------

DROP TABLE IF EXISTS `system_table`;

CREATE TABLE `system_table` (
  `id` varchar(50) NOT NULL,
  `field_name` varchar(50) DEFAULT NULL,
  `chinese_name` varchar(50) DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `displayable` int(11) DEFAULT NULL COMMENT '是否可显示,1：可显示，2：不可显示',
  `summation` int(11) DEFAULT NULL,
  `inputable` int(11) DEFAULT NULL,
  `filterable` int(11) DEFAULT NULL,
  `keyword` int(11) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `parent_field_name` varchar(50) DEFAULT NULL,
  `parent_chinese_name` varchar(50) DEFAULT NULL,
  `is_readonly` tinyint(4) DEFAULT NULL,
  `sortable` int(11) DEFAULT '0' COMMENT '是否可排序，1:可排序，0:不可排序',
  `is_del` bit(1) DEFAULT b'0' COMMENT '删除标记',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
```
