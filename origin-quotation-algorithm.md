## 原始报价算法总结
### 1.基础说明
#### 1.1 术语及特定说明
* 1.1.1 左右翻：即翻印，左右翻左边跟右边对称，它是第一面印完，印第二面时候纸张沿着左边翻到第二面
* 1.1.2 天地翻：是印第二面是纸张沿着上面边翻过来。对应印面纸张，都是长边对着机器上机的，因为宽边短，这样走纸快，效率高。
* 1.1.3 P数：即印刷页数，页数>=4可判断为样本，单页P数必定为2P
* 1.1.4 纸张规格：
* 全开 大度 1194×889
* 正度 1092×787
* 对开 大度 889×597
* 正度 787×546
* 3开 大度 889×398
* 正度 787×364
* 丁3开 大度 444×750
* 正度 393×698
* 4开 大度 597×444
* 正度 546×393
* 6开 大度 444×398
* 正度 393×364
* 8开 大度 444×298
* 1.1.5 排版优先：正反版优先，剩下的使用自翻版
* 1.1.6 最优排版数：经过排版计算得到的一个值
* 1.1.7 混拼：即长排和宽排可能同时出现的情形，暂不考虑是否指定混拼
* 1.1.8 isWebPressPaper:平张(相对于卷筒来说的)
* 1.1.9 机器排版：按机器排是选择最优的印刷工序和排版数；按材料排是选择材料规格和材料做几开，目前不考虑常规材料尺寸的排版，只考虑正度和大度的排版
* 1.1.10 分贴：暂不考虑是否指定分贴
* 1.1.10 所有工序(机器)依次排版之后，还需要依次分贴，原因是，每一贴适合的机器可能是有差异的。但是如果确定只有一贴，如单张，就不需要再次分贴
### 1.2报价参数
| 参数名称 | 参数说明 | 备注 |
| :---  |   :-------    |    :---   |
| productSortId   | 原始的产品分类id,因为可能外发，这个用来查上浮系数     |     |
| toProductSortId   | 产品分类ID(例如单页),外发的产品分类ID,真正进行报价的就是这个id     |     |
| productSortName   | 二级分类名称（例如单张类）     |     |
| modelName   | 模板名称（例如danzhang）     |     |
| styleId   | 产品规格id     |     |
| materialId   | 材料ID     |     |
| productMaterialName   | 材料名称，画册封面材料     |     |
| neiyeMaterialName   | 内页材料     |     |
| chayeMaterialName   | 插页材料     |     |
| neiyeIsQqual   | 内页内容一样     |     |
| productMaterialWeightId   | 材料克重id     |     |
| grameWeight   | 材料克重,和封面克重共用字段     |     |
| nwGrameWeight   | 内文克重     |     |
| cyGrameWeight   | 插页克重     |     |
| custId   | 客户ID     |     |
| productWidth   | 成品宽     |     |
| productLength   | 成品长     |     |
| productHeight   | 成品高     |     |
| sizeLength   | 包装盒类的展开长     |     |
| sizeWidth   | 包装盒类的展开宽     |     |
| wanbian   | 手提袋挽边     |     |
| nianbian   | 手提袋粘边     |     |
| zhedi   | 手提袋折底重叠     |     |
| niankou   | 手提袋粘口类型     |     |
| mode   | 印面，1.单面，2.双面     |     |
| color   | 印色，1.单色，2.双色，4.彩色     |     |
| productQty   | 总数量(便签本数，联单本数)     |     |
| kuanNum   | 名片款数     |     |
| doudiWidth   | 封套兜底宽度     |     |
| lianQty   | 联单联数     |     |
| perQty   | 名片每款盒数、便签每本页数、联单每本页数     |     |
| sortNum   | 单页种类     |     |
| nyPages   | 内页页数     |     |
| cyPages   | 插页页数     |     |
| fmPages   | 封面页数     |     |
| sPartInfo   | 个性化类的部件数组信息     |     |
| neiyePartInfo   | 画册类的内页部件信息     |     |
| chayePartInfo   | 画册类的插页部件信息     |     |
| yhProcess   | 印后工序要求，显示格式为 "复膜,局部UV{23*45},模切",参数为"78E2EEA0-13DF-4CDB-9411-39F54F32E81B,78E2EEA0-13DF-4CDB-9411-39F54F32E81B{23*45},78E2EEA0-13DF-4CDB-9411-39F54F32E81B"     |     |
| yhProcessName   | 印后工序名称     |     |
| cpProcess   | 成品工序要求，显示格式为 "骑马钉,穿线胶装,切成品",参数为"58911EF3-C47B-4DEC-B030-0111E30BE40D,6FBD8909-D08E-47C5-A005-3556CAB05DF3,6078E2A2-D89E-4A44-B8C3-129AB5EA1523"     |     |
| cpProcessName   | 成品工序名称     |     |
| deliverId   | 配送方式ID     |     |
| storeId   | 加工商ID     |     |
| custLevelId   | 客户等级ID     |     |
| arrivalId   | 交期方式ID     |     |
| productFactoryId   | 印刷工厂ID     |     |
| productFactoryName   | 印刷工厂名称     |     |
| isCommonPrice   | 是否是通用产品报价Y     |     |
| materialReprint   | 翻印，是否自翻Y/N     |     |


### 3.计算流程
* 2.1 整体流程
* 2.1.1 根据产品分类ID取产品分类
* 2.1.2 取产品分类名称
* 2.1.3 定义出血位：总，上，右，下，左。总出血适用于各位置出血位一致的，如果不一致则可能四个方位分别指定
* 2.1.4 初始化：总页数(allPage：2)，总金额(totalMoney:0f),是否计算成品工艺标记(countCppProcess:true),封面内页是否合并(iCombine:0)
* 2.1.3 初始化：分贴调试信息，错误信息，公式错误信息，印刷工序数量
```
List<String> tsInfo = new ArrayList<String>();
List<String> yhMoneyList = new ArrayList<String>();
List<String> cpMoneyList = new ArrayList<String>();
List<String> errInfoList = new ArrayList<String>();
List<String> formulaErrInfoList = new ArrayList<String>();
```
* 2.1.4 判断模板类型(如：danzhang(单张))
* 2.1.5 分贴
* 2.1.5.1 返回值：分贴对象列表 Map<String,List<Object>>
* 2.1.5.2 取产品规格：判断规格ID(styleId)不为空时查询产品规格，可获取规格名称
* 2.1.5.3 比较产品长宽，使其必须是长宽
```
Float tempLength = 0f;
Float tmpProductWidth = productWidth;
Float tmpProductLength = productLength;

if(tmpProductWidth > tmpProductLength){
	tempLength = tmpProductWidth;
	tmpProductWidth  = tmpProductLength;
	tmpProductLength = tempLength;
}
```
* 2.1.5.4 初始化分贴List
```
List<Object> ts = new ArrayList<Object>();
```
* 2.1.5.5 取材料单价,根据材料id及材料克重，如果材料设置中，材料克重表有记录，取克重表价格，否则取材料表价格
```
Float materialPrice = 0f;//从材料设置取
	ProductMaterial productMaterial = UtilValidate.isNotEmpty(materialId) ? productMaterialService.selectByPrimaryKey(materialId) : null;
			//delegator.findOne("ProductMaterial", false, UtilMisc.toMap("materialId",materialId));
	String materialMethod = "";
	String materialReprint = "";
	if(UtilValidate.isNotEmpty(productMaterial)){
		if(UtilValidate.isNotEmpty(grameWeight)){      //如果有克重
			ProductMaterialWeight productMaterialWeight = productMaterialService.getProductMaterialWeight(materialId,grameWeight);//从材料设置取
			if(null != productMaterialWeight){
				if(UtilValidate.isNotEmpty(productMaterialWeight.getTonnePriceNum()))
				materialPrice = productMaterialWeight.getTonnePriceNum().floatValue();
			}else{
				if(UtilValidate.isNotEmpty(productMaterial.getMaterialPrice()))
				materialPrice = productMaterial.getMaterialPrice().floatValue();
			}
		}else{ //没有克重直接取材料
			if(UtilValidate.isNotEmpty(productMaterial.getMaterialPrice()))
			materialPrice = productMaterial.getMaterialPrice().floatValue();
		}
		materialMethod = productMaterial.getMaterialMethod();//材料算法，仅存入so，以便后面计算材料费时调用
		materialReprint = "N".equals(productMaterial.getMaterialReprint()) ? "N" : "Y"; //由于老客户都没有值，现在判断为N时说明客户选择过了，为N，其他null “” y 都为 Y
	}
		
```
* 2.1.5.6 分贴及混拼声;
```
String sIsPaste = "Y";//是否分贴
String isBlending = "Y";//是否混拼
List<Object> composedProcessList = new ArrayList<Object>();//排版工序列表
List<ProductProcess> composedProcessAndConsList = new ArrayList<ProductProcess>();// 存取计算后为正反版（单页类）的 印刷工序，重新计算自翻版
```
* 2.1.5.7 辅料分贴
```
ProductProcess processForAccessory = new ProductProcess();
	processForAccessory.setMaxMachineWidth(new BigDecimal(productWidth));
	processForAccessory.setMaxMachineLength(new BigDecimal(productLength));
	processForAccessory.setdMachineWidth(new BigDecimal(productWidth));
	processForAccessory.setdMachineLength(new BigDecimal(productLength));
	processForAccessory.setdMaterialLength(new BigDecimal(productLength));
	processForAccessory.setdMaterialWidth(new BigDecimal(productWidth));
	processForAccessory.setiNumberofcomposing(new BigDecimal(1l));
	processForAccessory.setiNumberofMaterial(new BigDecimal(1l));
	processForAccessory.setsMethod("L"+"#"+"2"+"*"+"1");
	processForAccessory.setsMaterialMethod("L"+"#"+"1"+"*"+"1");
	processForAccessory.setiPrintType(new BigDecimal(3l));//单面印刷
	processForAccessory.setBaojiaFormulaId(baojiaFormulaId);
	composedProcessList.add(processForAccessory);

	result.put("composedProcessList", composedProcessList);
	so = new SplitObject();
	so = splitObjectCommonSets(so,productLength,productWidth,  materialPrice,nyPages ,cyPages ,fmPages,  allpages, sizeName,doudiWidth,perQty,lianQty,kuanNum,materialMethod);
      if("fm".equals(partsCategoryName)){
    	  so.setPages(new BigDecimal(fmPages));
      }else if("ny".equals(partsCategoryName)){
    	  so.setPages(new BigDecimal(nyPages));
      }else if("cy".equals(partsCategoryName)){
    	  so.setPages(new BigDecimal(cyPages));
      }else{
    	//页数固定为2，一张纸固定2页
          so.setPages(new BigDecimal(2));
      }
        //标识so为自定义尺寸
        if (UtilValidate.isNotEmpty(styleId) && styleId != "")
        	so.setIsUserDefinedSize("N");
        else
        	so.setIsUserDefinedSize("Y");
        so.setsIndex("1");
        ts.add(so);

      result.put("ts", ts);

      return result;
```
* 2.1.5.8 查找印刷工序：判断是否指定了印刷工序(printProcessId)，如果指定了则取指定的印刷工序(只会有一个印刷工序)，如果没有指定则去相应的印刷工序(会有多个印刷工序)
* 判断是否有部件Id(partsCategoryId)传过来，如果有就按部件id查，如果没有则取印件分类Id
* 未指定印刷工序则：
```
if(UtilValidate.isNotEmpty(partsCategoryId) && !"jingzhuanghe".equalsIgnoreCase(modelName)){
    	ysProcess = productProcessService.getMachineListForCategory(colors, 
    	(UtilValidate.isNotEmpty(grameWeight) && isDouble(grameWeight)) ? new BigDecimal(grameWeight) : new BigDecimal(0), 
    	filterCategoryId, partsCategoryId, storeId);//根据印件分类找对应的印刷工序分类
    }else{
    	ysProcess = productProcessService.getMachineListForCategory(colors, 
    	(UtilValidate.isNotEmpty(grameWeight) && isDouble(grameWeight)) ? new BigDecimal(grameWeight) : new BigDecimal(0), 
    	filterCategoryId, productSortId, storeId);//根据印件分类找对应的印刷工序分类
    }
```
* 指定了印刷工序则：
```
ProductProcess productProcess =productProcessService.selectByPrimaryKey(printProcessId);
if(UtilValidate.isNotEmpty(productProcess)){
	ysProcess.add(productProcess);
}
```
* 如果始终无法找到印刷工序则：
* 报错返回，因为印刷工序是必须的
* 2.1.5.9 获取各种印刷的排版，开料，印刷方式 等信息， 后面要用到
* 按开数从大到小的次序,计算出满足条件的拼版对象，并存下所有的值---循环上面得到的印刷工序List
* 查询印刷工序是否存在常用上机规格
```
List<ProcessRegularStyle> machineRegularStyleList = productProcessService.getProcessRegularStyle(process.getProcessId());
```
* 如果没有定义常规尺寸：
```
if(machineRegularStyleList.isEmpty()){
    //如果产品尺寸小于最大上机尺寸
    if(tmpProductLength <= process.getMaxMachineLength().floatValue() && tmpProductWidth <= process.getMaxMachineWidth().floatValue()){
        //则排版
        ProductProcess newProcess = calcComposing(process, productLength,productWidth,pages,sDs,materialId,materialWeightId,cpProcessName, modelName, productSortId, isBlending);
    }
}
```
* 如果定义了常规尺寸：
```
if(tmpProductLength <= process.getMaxMachineLength().floatValue()
  		&& tmpProductWidth <= process.getMaxMachineWidth().floatValue()){
  		//即使配了常规尺寸，也有可能遇到不常规的产品尺寸，所以还是要用特规尺寸参与运算
		calcMachineComposing(process, productLength,productWidth,pages,sDs,materialId,grameWeight, modelName,cpProcessName, isBlending);
		tempUtilization = process.getMachineUtilization().floatValue();
		tempProcess = process;
  }
  //初始化利用率
  Map<String,ProductProcess> allUtilization = new HashMap<String,ProductProcess>();
  //遍历常规尺寸列表
  for(ProcessRegularStyle pstyle : machineRegularStyleList){
  	float styleLength = pstyle.getdMachineLength().floatValue();
  	float styleWidth = pstyle.getdMachineWidth().floatValue();
  	ProductProcess regularProcess=new ProductProcess();
	CopyProperties.copyPropertiesExclude(process, regularProcess, null);
  	//GenericValue regularProcess = (GenericValue)process.clone();
  	regularProcess.setMaxMachineLength(BigDecimal.valueOf(styleLength));
  	regularProcess.setMaxMachineWidth(BigDecimal.valueOf(styleWidth));
  	//以此按常规尺寸排版
  	calcMachineComposing(regularProcess, productLength,productWidth,pages,sDs,materialId,grameWeight, modelName,cpProcessName, isBlending);

  	String sStyle = String.valueOf(styleLength) + "*" + String.valueOf(styleWidth);
  	allUtilization.put(sStyle, regularProcess);

  	if(tempUtilization <= regularProcess.getMachineUtilization().floatValue()){
  		tempUtilization = regularProcess.getMachineUtilization().floatValue();
			tempProcess = regularProcess;
			sMaxStyle = sStyle;
  	}

  }
```

* 




* 2.2 分贴
* 2.1.1 分贴返回：
* 2.1.2 分贴流程
* 取材料单价,根据材料id及材料克重(取材料价格：如果材料设置中，材料克重表有记录，取克重表价格，否则取材料表价格)
* 2
* 2.2 排版
* 2.2.1 按机器排版
* 2.2.2 按材料排版
* 
* 其它记录：
* 1.单页类双面印刷，支持混拼，即长排和宽排可能同时出现的情形









